"""Console script for barzahlen_python."""
import configparser
import json
import os
from pathlib import Path
import sys
import uuid

import click

from .client import BarzahlenAPIClient
from .config import CLI_CONFIG_FILE
from .middleware import Middleware
from . import error
from . import utils

CONTEXT_SETTINGS = dict(help_option_names=["-h", "--help"], max_content_width=120)
CONFIG_FILE_PATH = f"{str(Path.home())}/{CLI_CONFIG_FILE}"


@click.group(context_settings=CONTEXT_SETTINGS)
@click.version_option()
def main():  # pragma: no cover
    ctx = click.get_current_context()
    ctx.obj = {}
    if os.path.isfile(CONFIG_FILE_PATH):
        conf = configparser.ConfigParser()
        conf.read(CONFIG_FILE_PATH)
        if "BARZAHLEN" in conf:
            if all(k in conf["BARZAHLEN"] for k in ("BZ_API_KEY", "BZ_DIVISION_ID")):
                ctx.obj = {
                    "api_key": conf.get("BARZAHLEN", "BZ_API_KEY"),
                    "stores_api_key": conf.get("BARZAHLEN", "BZ_STORES_API_KEY"),
                    "division_id": conf.get("BARZAHLEN", "BZ_DIVISION_ID"),
                }

    if ctx.invoked_subcommand is None:
        click.echo(ctx.get_help())


def _default_from_context(default_name):
    class OptionDefaultFromContext(click.Option):
        def get_default(self, ctx, call=True):  # pragma: no cover
            self.default = ctx.obj[default_name]
            return super(OptionDefaultFromContext, self).get_default(ctx, call)

    return OptionDefaultFromContext


def echo_header(title, color):
    total_width = 30
    padding = int(total_width / 2 - (len(title) / 2) - 1)
    click.echo(click.style("#" * total_width, fg=color))
    click.echo(click.style("#" + " " * padding + title + " " * padding + "#", fg=color))
    click.echo(click.style("#" * total_width, fg=color))


def echo_response(response):
    click.echo(click.style("URL", bg="white", fg="black"))
    click.echo(response.get_url())
    click.echo(click.style("String to sign", bg="white", fg="black"))
    click.echo(response.get_string_to_sign())
    click.echo(click.style("Headers", bg="white", fg="black"))
    click.echo(json.dumps(response.get_headers(), indent=4))
    click.echo(click.style("Response status code", bg="white", fg="black"))
    click.echo(response.get_response_status_code())
    click.echo(click.style("Response content", bg="white", fg="black"))
    click.echo(json.dumps(response.get_response_content_as_json(), indent=4))


@click.command(help="Test internal algorithm to generate Barzahlen API signature")
@click.option("--api-key", cls=_default_from_context("api_key"), prompt="API key")
@click.option(
    "--host-header", default="api-sandbox.barzahlen.de:443", prompt="Host:port"
)
@click.option("--host-path", default="/v2/slips", prompt="Endpoint")
@click.option("--date-header", default=utils.http_date_now(), prompt="Date header")
@click.option("--idempotency-key", default=str(uuid.uuid4()), prompt="Idempotency key")
@click.option("--customer-key", default="customer@example.com", prompt="Customer key")
@click.option("--amount", default="99.50", prompt="Payment amount")
@click.option("--currency", default="EUR", prompt="Payment currency")
def generate_signature(
    api_key,
    host_header,
    host_path,
    date_header,
    idempotency_key,
    customer_key,
    amount,
    currency,
):
    echo_header("Calculate BZ signature", "yellow")

    request_body = {
        "slip_type": "payment",
        "customer": {"key": customer_key},
        "transactions": [{"currency": currency, "amount": amount}],
    }
    string_to_sign, signature = Middleware.generate_bz_signature(
        api_key,
        host_header,
        "POST",
        date_header,
        host_path,
        "",
        idempotency_key,
        json.dumps(request_body),
    )
    click.echo(click.style("String to sign", bg="white", fg="black"))
    click.echo(f"{string_to_sign}")
    click.echo(click.style("Signature", bg="white", fg="black"))
    click.echo(f"{signature}")


@click.command(help="Check service availability and authentication headers calculation")
@click.option("--api-key", cls=_default_from_context("api_key"), prompt="API key")
@click.option(
    "--division-id", cls=_default_from_context("division_id"), prompt="Division Id"
)
def ping(api_key, division_id):
    """Prompt for parameters"""
    echo_header("Ping service", "yellow")

    client = BarzahlenAPIClient(division_id, api_key, sandbox=True)
    ping_response = client.ping()
    echo_response(ping_response)
    click.echo(click.style("Service UP. Authentication successful", fg="green"))


@click.command(help="Create a payment, partial payment, payout or refund slip")
@click.option("--api-key", cls=_default_from_context("api_key"), prompt="API key")
@click.option(
    "--division-id", cls=_default_from_context("division_id"), prompt="Division Id"
)
@click.option("--slip-type", "-t", required=True, type=str)
@click.option("--customer-key", "-k", required=True, type=str)
@click.option("--amount", "-a", required=True, type=str)
@click.option("--currency", "-c", required=False, type=str)
@click.option("--email", "-e", required=False, type=str)
@click.option("--hook-url", "-h", required=False, type=str)
def create_slip(
    api_key,
    division_id,
    slip_type,
    customer_key,
    amount,
    currency="EUR",
    email=None,
    hook_url=None,
):
    """Create a slip"""
    echo_header("Create slip", "yellow")

    client = BarzahlenAPIClient(division_id, api_key, sandbox=True)

    try:
        if slip_type == "payment":
            if hook_url is None:
                slip_response = client.create_payment(
                    customer_key, float(amount), currency, email=email
                )
            else:
                slip_response = client.create_payment(
                    customer_key,
                    float(amount),
                    currency,
                    email=email,
                    hook_url=hook_url,
                )
        elif slip_type == "payout":
            if hook_url is None:
                slip_response = client.create_payout(
                    customer_key, float(amount), currency, email=email
                )
            else:
                slip_response = client.create_payout(
                    customer_key,
                    float(amount),
                    currency,
                    email=email,
                    hook_url=hook_url,
                )
        else:
            click.echo(click.style("Slip type not supported", fg="red"))
            sys.exit(1)
    except (error.BarzahlenAPIClientError, error.ApiError) as err:
        click.echo(click.style("There was an error creating a slip", fg="red"))
        click.echo(click.style(str(err), fg="red"))
        return

    echo_response(slip_response)
    click.echo(click.style("Payment transaction succesfully created", fg="green"))
    click.echo(
        click.style("Slip number: " + slip_response.get_slip_number(), fg="green")
    )


@click.command(help="Invalidate a slip")
@click.option("--api-key", cls=_default_from_context("api_key"), prompt="API key")
@click.option(
    "--division-id", cls=_default_from_context("division_id"), prompt="Division Id"
)
@click.option("--slip-id", "-s", required=True, type=str)
def invalidate_slip(api_key, division_id, slip_id):
    """Invalidate a slip"""
    echo_header("Invalidate slip", "yellow")

    client = BarzahlenAPIClient(division_id, api_key, sandbox=True)

    try:
        slip_response = client.invalidate_slip(slip_id)
    except (error.BarzahlenAPIClientError, error.ApiError) as err:
        click.echo(click.style("There was an error invalidating a slip", fg="red"))
        click.echo(click.style(str(err), fg="red"))
        return

    echo_response(slip_response)
    click.echo(click.style("Slip invalidated", fg="green"))
    click.echo(
        click.style("Slip number: " + slip_response.get_slip_number(), fg="green")
    )


@click.command(help="Retrieve a slip")
@click.option("--api-key", cls=_default_from_context("api_key"), prompt="API key")
@click.option(
    "--division-id", cls=_default_from_context("division_id"), prompt="Division Id"
)
@click.option("--slip-id", "-s", required=True, type=str)
def retrieve_slip(api_key, division_id, slip_id):
    """Retrieve a slip"""
    echo_header("Retrieve slip", "yellow")

    client = BarzahlenAPIClient(division_id, api_key, sandbox=True)

    try:
        slip_response = client.retrieve_slip(slip_id)
    except (error.BarzahlenAPIClientError, error.ApiError) as err:
        click.echo(click.style("There was an error retrieving a slip", fg="red"))
        click.echo(click.style(str(err), fg="red"))
        return

    echo_response(slip_response)
    click.echo(click.style("Slip retrieved", fg="green"))
    click.echo(
        click.style("Slip number: " + slip_response.get_slip_number(), fg="green")
    )


@click.command(help="Update a slip")
@click.option("--api-key", cls=_default_from_context("api_key"), prompt="API key")
@click.option(
    "--division-id", cls=_default_from_context("division_id"), prompt="Division Id"
)
@click.option("--slip-id", "-s", required=True, type=str)
@click.option("--amount", "-a", required=False, type=str)
@click.option("--cell-phone", "-c", required=False, type=str)
@click.option("--email", "-e", required=False, type=str)
@click.option("--reference-key", "-r", required=False, type=str)
def update_slip(
    api_key,
    division_id,
    slip_id,
    amount=None,
    cell_phone=None,
    email=None,
    reference_key=None,
):
    """Update a slip"""
    echo_header("Update slip", "yellow")

    client = BarzahlenAPIClient(division_id, api_key, sandbox=True)

    try:
        amount = float(amount) if amount is not None else None
        slip_response = client.update_slip(
            slip_id,
            amount=amount,
            customer_cell_phone=cell_phone,
            customer_email=email,
            reference_key=reference_key,
        )
    except (error.BarzahlenAPIClientError, error.ApiError) as err:
        click.echo(click.style("There was an error updating a slip", fg="red"))
        click.echo(click.style(str(err), fg="red"))
        return

    echo_response(slip_response)
    click.echo(click.style("Slip updated", fg="green"))
    click.echo(
        click.style("Slip number: " + slip_response.get_slip_number(), fg="green")
    )


@click.command(help="Get nearby stores from lat/lng coordinates")
@click.option("--api-key", cls=_default_from_context("api_key"), prompt="API key")
@click.option(
    "--stores-api-key",
    cls=_default_from_context("stores_api_key"),
    prompt="Stores API key",
)
@click.option(
    "--division-id", cls=_default_from_context("division_id"), prompt="Division Id"
)
@click.option("--lat", "-l", required=True, type=str)
@click.option("--lng", "-g", required=False, type=str)
def stores_nearby(api_key, stores_api_key, division_id, lat, lng):
    """Stores nearby"""
    echo_header("Stores nearby", "yellow")

    client = BarzahlenAPIClient(
        division_id, api_key, stores_api_key=stores_api_key, sandbox=True
    )

    try:
        stores_response = client.get_nearby_stores(lat, lng)
    except (error.BarzahlenAPIClientError, error.ApiError) as err:
        click.echo(click.style("There was an error retrieving nearby stores", fg="red"))
        click.echo(click.style(str(err), fg="red"))
        return

    echo_response(stores_response)
    click.echo(
        click.style(
            "Successfully retrieved the 10 nearest stores from provided location",
            fg="green",
        )
    )


@click.command(help="Get stores within bounds")
@click.option("--api-key", cls=_default_from_context("api_key"), prompt="API key")
@click.option(
    "--stores-api-key",
    cls=_default_from_context("stores_api_key"),
    prompt="Stores API key",
)
@click.option(
    "--division-id", cls=_default_from_context("division_id"), prompt="Division Id"
)
@click.option("--top-left-lat", "-t", required=True, type=str)
@click.option("--top-left-lng", "-l", required=True, type=str)
@click.option("--bottom-right-lat", "-b", required=True, type=str)
@click.option("--bottom-right-lng", "-r", required=True, type=str)
def stores_within_bounds(
    api_key,
    stores_api_key,
    division_id,
    top_left_lat,
    top_left_lng,
    bottom_right_lat,
    bottom_right_lng,
):
    """Stores within bounds"""
    echo_header("Stores within bounds", "yellow")

    client = BarzahlenAPIClient(
        division_id, api_key, stores_api_key=stores_api_key, sandbox=True
    )

    try:
        stores_response = client.get_stores_within_bounds(
            top_left_lat, top_left_lng, bottom_right_lat, bottom_right_lng
        )
    except (error.BarzahlenAPIClientError, error.ApiError) as err:
        click.echo(
            click.style("There was an error retrieving stores within bounds", fg="red")
        )
        click.echo(click.style(str(err), fg="red"))
        return

    echo_response(stores_response)
    click.echo(
        click.style("Successfully retrieved the stores within bounds", fg="green")
    )


@click.command(help="Stores API credentials for subsequent commands")
@click.option(
    "--api-key",
    default=os.getenv("BZ_API_KEY"),
    prompt="Please enter your Barzahlen API key",
)
@click.option(
    "--stores-api-key",
    default=os.getenv("BZ_STORES_API_KEY"),
    prompt="Please enter your Barzahlen Stores API key",
)
@click.option(
    "--division-id",
    default=os.getenv("BZ_DIVISION_ID"),
    prompt="Please enter your Division Id",
)
@click.option("--config-file", default=CONFIG_FILE_PATH)
def config(api_key, stores_api_key, division_id, config_file):
    """
    Store configuration values in a file, e.g. the API key and Division Id.
    """
    echo_header("Save API credentials", "yellow")

    with open(config_file, "w", encoding="utf-8") as f:
        f.writelines(
            [
                "[BARZAHLEN]\n",
                f"BZ_API_KEY={api_key}\n",
                f"BZ_STORES_API_KEY={stores_api_key}\n",
                f"BZ_DIVISION_ID={division_id}\n",
            ]
        )

    click.echo(click.style("Credentials saved to: " + config_file, fg="green"))


main.add_command(config)
main.add_command(generate_signature)
main.add_command(ping)
main.add_command(create_slip)
main.add_command(invalidate_slip)
main.add_command(retrieve_slip)
main.add_command(update_slip)
main.add_command(stores_nearby)
main.add_command(stores_within_bounds)

if __name__ == "__main__":
    sys.exit(main())  # pragma: no cover
