# -*- coding: utf-8 -*-

from .validators import BarzahlenResponseValidator


class BarzahlenResponse:
    """Generic response"""

    def __init__(self, url, string_to_sign, headers, response):
        BarzahlenResponseValidator(response).validate()
        self._url = url
        self._string_to_sign = string_to_sign
        self._headers = headers
        self._response = response

    def get_url(self):
        return self._url

    def get_string_to_sign(self):
        return self._string_to_sign

    def get_headers(self):
        return self._headers

    def get_response_status_code(self):
        return self._response.status_code

    def get_response_content_as_json(self):
        return self._response.json()


class CreateSlipResponse(BarzahlenResponse):
    def get_slip_number(self):
        return self._response.json()["id"]


class InvalidateSlipResponse(CreateSlipResponse):
    pass


class RetrieveSlipResponse(CreateSlipResponse):
    pass


class UpdateSlipResponse(CreateSlipResponse):
    pass


class StoresNearbyResponse(BarzahlenResponse):
    pass


class StoresWithinBoundsResponse(BarzahlenResponse):
    pass
