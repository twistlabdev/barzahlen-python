# -*- coding: utf-8 -*-
from datetime import datetime
from urllib.parse import urlparse


def run_with_locale(catstr, *locales):  # pragma: no cover
    def decorator(func):
        def inner(*args, **kwds):
            try:
                import locale  # pylint: disable=import-outside-toplevel

                category = getattr(locale, catstr)
                orig_locale = locale.setlocale(category)
            except AttributeError:
                # if the test author gives us an invalid category string
                raise
            except:  # pylint: disable=bare-except
                # cannot retrieve original locale, so do nothing
                locale = orig_locale = None
            else:
                for loc in locales:
                    try:
                        locale.setlocale(category, loc)
                        break
                    except:  # pylint: disable=bare-except
                        pass
            # now run the function, resetting the locale on exceptions
            try:
                return func(*args, **kwds)
            finally:
                if locale and orig_locale:
                    locale.setlocale(category, orig_locale)

        inner.__name__ = func.__name__
        inner.__doc__ = func.__doc__
        return inner

    return decorator


@run_with_locale("LC_ALL", "en_US")
def http_date_now():
    """
    Return a string representation of a date according to RFC 1123
    (HTTP/1.1).

    The supplied date must be in UTC.
    """
    return datetime.utcnow().strftime("%a, %d %b %Y %H:%M:%S GMT")


def api_url_parse(api_url):
    """
    Return <network location>:<port> parsed from a standard URL
    """
    parsed_url = urlparse(api_url)
    return f"{parsed_url.netloc}:{'80' if parsed_url.scheme == 'http' else '443'}"
