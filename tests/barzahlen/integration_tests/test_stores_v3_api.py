from unittest import TestCase

import responses

from barzahlen import error
from barzahlen.client import BarzahlenAPIClient
from tests.fixtures.barzhalen_responses import nearest_stores_by_gps_location
from tests.fixtures.barzahlen_errors import invalid_point_parameters_error


class ApiBarzahlenAPIStoresV3TestCase(TestCase):
    def test_stores_api_key_is_none_in_nearby(self):
        client = BarzahlenAPIClient(
            "test", "test", sandbox=True
        )  # no stores api key is provided
        with self.assertRaises(error.IncorrectApiParametersError):
            client.get_nearby_stores("46.552746575072426", "6.664097368195876")

    def test_stores_nearby_lat_is_none(self):
        client = BarzahlenAPIClient("test", "test", "stores_api_key", sandbox=True)
        with self.assertRaises(error.IncorrectApiParametersError):
            client.get_nearby_stores(None, "6.664097368195876")

    def test_stores_within_bounds_lat_is_none(self):
        client = BarzahlenAPIClient("test", "test", "stores_api_key", sandbox=True)
        with self.assertRaises(error.IncorrectApiParametersError):
            client.get_stores_within_bounds(
                None, "6.664097368195876", None, "6.664097368195876"
            )

    @responses.activate
    def test_get_stores_nearby(self):
        latitude = "46.56210727859177"
        longitude = "6.610779506027727"
        response_body = nearest_stores_by_gps_location()
        responses.add(
            responses.GET,
            f"https://stores.viafintech.com/v3/stores/nearby?lat={latitude}&lng={longitude}",
            json=response_body,
            status=200,
        )
        client = BarzahlenAPIClient("test", "payment_key", "stores_key", sandbox=True)
        stores_response = client.get_nearby_stores(latitude, longitude)
        self.assertEqual(stores_response.get_response_status_code(), 200)

    @responses.activate
    def test_get_stores_within_bounds(self):
        top_left_lat = "46.552746575072426"
        top_left_lng = "6.600405404779666"
        bottom_right_lat = "46.51637469033725"
        bottom_right_lng = "6.66409736819587"
        response_body = nearest_stores_by_gps_location()
        responses.add(
            responses.GET,
            (
                f"https://stores.viafintech.com/v3/stores/within_bounds?"
                f"top_left_lat={top_left_lat}"
                f"&top_left_lng={top_left_lng}"
                f"&bottom_right_lat={bottom_right_lat}"
                f"&bottom_right_lng={bottom_right_lng}"
            ),
            json=response_body,
            status=200,
        )
        client = BarzahlenAPIClient("test", "payment_key", "stores_key", sandbox=True)
        stores_response = client.get_stores_within_bounds(
            top_left_lat, top_left_lng, bottom_right_lat, bottom_right_lng
        )
        self.assertEqual(stores_response.get_response_status_code(), 200)
