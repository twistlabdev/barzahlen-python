def create_payment_response():
    return {
        "id": "slp-7a89271d-33f4-489a-aa12-d4fb384b8aec",
        "slip_type": "payment",
        "division_id": "55894",
        "reference_key": "null",
        "hook_url": "null",
        "expires_at": "2020-08-18T21:59:59.000Z",
        "customer": {
            "key": "luis1234",
            "cell_phone_last_4_digits": "null",
            "email": "null",
            "language": "de-DE",
        },
        "checkout_token": "djF8Y2hrdHxzbHAtN2E4OTI3MWQtMzNmNC00ODlhLWFhMTItZDRmYjM4NGI4YWVjfERqaitTTTVnaGVSVUVoR1FFWXl"
        "UMlVDVXBZeGNUWERmbFVWTE02bTg2TW89",
        "metadata": {},
        "transactions": [
            {
                "id": "10337802",
                "currency": "EUR",
                "amount": "50.0",
                "displayed_due_at": "2020-08-18T21:59:59.000Z",
                "state": "pending",
                "country": "null",
            }
        ],
        "nearest_stores": [],
    }


def create_payout_response():
    return {
        "id": "slp-7a89271d-33f4-489a-aa12-d4fb384b8aec",
        "slip_type": "payout",
        "division_id": "55894",
        "reference_key": "null",
        "hook_url": "null",
        "expires_at": "2020-08-18T21:59:59.000Z",
        "customer": {
            "key": "luis1234",
            "cell_phone_last_4_digits": "null",
            "email": "null",
            "language": "de-DE",
        },
        "checkout_token": "djF8Y2hrdHxzbHAtN2E4OTI3MWQtMzNmNC00ODlhLWFhMTItZDRmYjM4NGI4YWVjfERqaitTTTVnaGVSVUVoR1FFWXl"
        "UMlVDVXBZeGNUWERmbFVWTE02bTg2TW89",
        "metadata": {},
        "transactions": [
            {
                "id": "10337802",
                "currency": "EUR",
                "amount": "-50.0",
                "displayed_due_at": "2020-08-18T21:59:59.000Z",
                "state": "pending",
                "country": "null",
            }
        ],
        "nearest_stores": [],
    }


def invalid_transactions_amount_error():
    return {
        "error_class": "invalid_parameter",
        "error_code": "invalid_transactions_amount",
        "message": "transaction.0.amount: Does not match pattern '^-?[0-9]+\\.[0-9]+$'",
        "request_id": "1865359ad354441b9403c3771d811c53",
    }


def invalidated_slip_response():
    return {
        "id": "slp-738c5997-fa25-4e5c-8665-eaed0e91d45f",
        "slip_type": "payment",
        "division_id": "55894",
        "reference_key": "null",
        "barcode_ean13": "null",
        "hook_url": "https://bae29cbf47fb.ngrok.io/api/webhooks/xxx-aaa-bbb-cccc/",
        "expires_at": "2020-09-20T21:59:59.000Z",
        "customer": {
            "key": "luistest",
            "cell_phone_last_4_digits": "null",
            "email": "null",
            "language": "de-DE",
        },
        "metadata": {},
        "transactions": [
            {
                "id": "10314157",
                "currency": "EUR",
                "amount": "100.0",
                "displayed_due_at": "2020-09-20T21:59:59.000Z",
                "state": "invalidated",
                "country": "null",
            }
        ],
        "nearest_stores": [],
    }


def retrieve_invalidated_slip_response():
    return {
        "id": "slp-31ab7b60-3df8-4979-8045-8b1af0bee895",
        "slip_type": "payment",
        "division_id": "55894",
        "reference_key": "null",
        "barcode_ean13": "null",
        "hook_url": "https://bae29cbf47fb.ngrok.io/api/webhooks/xxx-aaa-bbb-cccc/",
        "expires_at": "2020-09-20T21:59:59.000Z",
        "customer": {
            "key": "luistest",
            "cell_phone_last_4_digits": "null",
            "email": "null",
            "language": "de-DE",
        },
        "metadata": {},
        "transactions": [
            {
                "id": "10942597",
                "currency": "EUR",
                "amount": "100.0",
                "displayed_due_at": "2020-09-20T21:59:59.000Z",
                "state": "invalidated",
                "country": "null",
            }
        ],
        "nearest_stores": [],
    }


def updated_slip_response():
    return {
        "id": "slp-d8d2b41e-fd55-48fc-a1e3-b6bbc0c246e2",
        "slip_type": "payment",
        "division_id": "55894",
        "reference_key": "mykey",
        "barcode_ean13": "4053101505048",
        "hook_url": "https://bae29cbf47fb.ngrok.io/api/webhooks/xxx-aaa-bbb-cccc/",
        "expires_at": "2020-09-25T21:59:59.000Z",
        "customer": {
            "key": "luistest",
            "cell_phone_last_4_digits": "null",
            "email": "luis.rodriguez@twistlab.ch",
            "language": "de-DE",
        },
        "metadata": {},
        "transactions": [
            {
                "id": "10150504",
                "currency": "EUR",
                "amount": "50.0",
                "displayed_due_at": "2020-09-25T21:59:59.000Z",
                "state": "pending",
                "country": "null",
            }
        ],
        "nearest_stores": [],
    }


def retrieved_slip_during_update_response():
    return {
        "id": "slp-d8d2b41e-fd55-48fc-a1e3-b6bbc0c246e2",
        "slip_type": "payment",
        "division_id": "55894",
        "reference_key": "mykey",
        "barcode_ean13": "4053101505048",
        "hook_url": "https://bae29cbf47fb.ngrok.io/api/webhooks/xxx-aaa-bbb-cccc/",
        "expires_at": "2020-09-25T21:59:59.000Z",
        "customer": {
            "key": "luistest",
            "cell_phone_last_4_digits": "null",
            "email": "luis.rodriguez@twistlab.ch",
            "language": "de-DE",
        },
        "metadata": {},
        "transactions": [
            {
                "id": "10150504",
                "currency": "EUR",
                "amount": "50.0",
                "displayed_due_at": "2020-09-25T21:59:59.000Z",
                "state": "pending",
                "country": "null",
            }
        ],
        "nearest_stores": [],
    }


def nearest_stores_by_gps_location():
    return [
        {
            "id": "20792",
            "lat": "46.56246993",
            "lng": "6.603168457",
            "street_and_no": "",
            "phone": "",
            "parking": "false",
            "logo_url": "https://cdn.viafintech.com/images/filialfinder/logo_sbb.png",
            "logo_thumbnail_url": "https://cdn.viafintech.com/images/filialfinder/tn_sbb.png",
            "minutes_until_close": "506",
            "offline_partner_id": "63147",
            "distance": "0.58",
            "title": "Am Billettautomat, Romanel-sur-Lausanne",
            "zipcode": "",
            "city": "",
            "countrycode": "CH",
            "opening_hours_per_day": [
                {"day": "sun", "open": [{"begin": "00:00", "end": "23:59"}]},
                {"day": "mon", "open": [{"begin": "00:00", "end": "23:59"}]},
                {"day": "tue", "open": [{"begin": "00:00", "end": "23:59"}]},
                {"day": "wed", "open": [{"begin": "00:00", "end": "23:59"}]},
                {"day": "thu", "open": [{"begin": "00:00", "end": "23:59"}]},
                {"day": "fri", "open": [{"begin": "00:00", "end": "23:59"}]},
                {"day": "sat", "open": [{"begin": "00:00", "end": "23:59"}]},
            ],
        },
        {
            "id": "20793",
            "lat": "46.55687572",
            "lng": "6.600907411",
            "street_and_no": "",
            "phone": "",
            "parking": "false",
            "logo_url": "https://cdn.viafintech.com/images/filialfinder/logo_sbb.png",
            "logo_thumbnail_url": "https://cdn.viafintech.com/images/filialfinder/tn_sbb.png",
            "minutes_until_close": "506",
            "offline_partner_id": "63147",
            "distance": "0.95",
            "title": "Am Billettautomat, Le Lussex",
            "zipcode": "",
            "city": "",
            "countrycode": "CH",
            "opening_hours_per_day": [
                {"day": "sun", "open": [{"begin": "00:00", "end": "23:59"}]},
                {"day": "mon", "open": [{"begin": "00:00", "end": "23:59"}]},
                {"day": "tue", "open": [{"begin": "00:00", "end": "23:59"}]},
                {"day": "wed", "open": [{"begin": "00:00", "end": "23:59"}]},
                {"day": "thu", "open": [{"begin": "00:00", "end": "23:59"}]},
                {"day": "fri", "open": [{"begin": "00:00", "end": "23:59"}]},
                {"day": "sat", "open": [{"begin": "00:00", "end": "23:59"}]},
            ],
        },
        {
            "id": "20794",
            "lat": "46.57093822",
            "lng": "6.60479872",
            "street_and_no": "",
            "phone": "",
            "parking": "false",
            "logo_url": "https://cdn.viafintech.com/images/filialfinder/logo_sbb.png",
            "logo_thumbnail_url": "https://cdn.viafintech.com/images/filialfinder/tn_sbb.png",
            "minutes_until_close": "506",
            "offline_partner_id": "63147",
            "distance": "1.08",
            "title": "Am Billettautomat, Vernand-Camar\u00e8s",
            "zipcode": "",
            "city": "",
            "countrycode": "CH",
            "opening_hours_per_day": [
                {"day": "sun", "open": [{"begin": "00:00", "end": "23:59"}]},
                {"day": "mon", "open": [{"begin": "00:00", "end": "23:59"}]},
                {"day": "tue", "open": [{"begin": "00:00", "end": "23:59"}]},
                {"day": "wed", "open": [{"begin": "00:00", "end": "23:59"}]},
                {"day": "thu", "open": [{"begin": "00:00", "end": "23:59"}]},
                {"day": "fri", "open": [{"begin": "00:00", "end": "23:59"}]},
                {"day": "sat", "open": [{"begin": "00:00", "end": "23:59"}]},
            ],
        },
    ]
