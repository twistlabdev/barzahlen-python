def transaction_paid_webhook_request():
    return {
        "Bz-Hook-Format": "v2",
        "Host": "callback.example.com",
        "Path": "/barzahlen/callback",
        "Port": "443",
        "Date": "Fri, 01 Apr 2016 09:20:06 GMT",
        "Method": "POST",
        "Bz-Signature": "BZ1-HMAC-SHA256 7fee59f32e573f5a732ad26331044b95bb6b36c2b75ba1a61185ec967485e94d",
        "Body": (
            '{"event": "paid", "event_occurred_at": "2016-01-06T12:34:56Z", '
            '"affected_transaction_id": "4729294329", "slip": {"id": '
            '"slp-d90ab05c-69f2-4e87-9972-97b3275a0ccd", "slip_type": "payment", '
            '"division_id": "1234", "reference_key": "O64737X", "hook_url": "null", '
            '"expires_at": "2016-01-10T12:34:56Z", "customer": {"key": "LDFKHSLFDHFL", '
            '"cell_phone_last_4_digits": "6789", "email": "john@example.com", "language": '
            '"de-DE"}, "metadata": {"order_id": "1234", "invoice_no": "A123"}, '
            '"transactions": [{"id": "4729294329", "currency": "EUR", "amount": "123.34", '
            '"state": "paid", "displayed_due_at": "2016-01-10T10:00:00Z"}], '
            '"nearest_stores": []}}'
        ),
    }
