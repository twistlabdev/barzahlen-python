# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).


## [1.0.8] - 2022-02-09

### Improved
- Loosen dependency
- pep8/blacked


## [1.0.7] - 2021-07-14

### Added

- Implement new v3 Stores API endpoints

### Refs
- [Issue #10](https://gitlab.com/twistlabdev/barzahlen-python/-/issues/10)

## [1.0.6] - 2020-09-13

### Added

- Support for creating payout type of slips

### Improved

- Added definition of specific errors returned by the update slip endpoint

### Refs
- [Issue #1](https://gitlab.com/twistlabdev/barzahlen-python/-/issues/1)


## [1.0.5] - 2020-09-11

### Added

- Update slip

### Refs
- [Issue #3](https://gitlab.com/twistlabdev/barzahlen-python/-/issues/3)

## [1.0.4] - 2020-09-08

### Added

- Invalidate slip
- Retrieve slip

### Refs
- [Issue #2](https://gitlab.com/twistlabdev/barzahlen-python/-/issues/2)
- [Issue #5](https://gitlab.com/twistlabdev/barzahlen-python/-/issues/5)

## [1.0.3] - 2020-08-28

### Fixed

- Webhook signatures are not correctly validated

### Refs
- [Issue #9](https://gitlab.com/twistlabdev/barzahlen-python/-/issues/9)


## [1.0.2] - 2020-08-20

### Added

- Method to validate webhook request signature

### Refs
- [Issue #8](https://gitlab.com/twistlabdev/barzahlen-python/-/issues/8)

## [1.0.1] - 2020-08-07

### Improved

- Localize generation of HTTP date included in Authorization headers

## [1.0.0] - 2020-08-07

### Added
- Initial commit, first working version.
- Ping operation to check service availability
- Create payment (slip with slip_type='payment')

