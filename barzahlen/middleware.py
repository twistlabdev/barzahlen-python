# -*- coding: utf-8 -*-
import base64
import hashlib
import hmac
import json
import uuid

from . import utils


class Middleware:
    @staticmethod
    def generate_bz_signature(
        payment_key,
        request_host_header,
        request_method,
        request_date_header,
        request_host_path,
        request_query_string,
        request_idempotency_key,
        request_body,
    ):

        request_body_hash = hashlib.sha256(request_body.encode("utf-8"))
        signature_data = [
            request_host_header,
            request_method.upper(),
            request_host_path,
            request_query_string,
            request_date_header,
            request_idempotency_key,
            request_body_hash.hexdigest(),
        ]

        signature_string = "\n".join(signature_data)
        signature = hmac.new(
            bytes(payment_key, "utf-8"),
            bytes(signature_string, "utf-8"),
            getattr(hashlib, "sha256"),
        ).hexdigest()
        return signature_string, signature

    @staticmethod
    def build_headers(
        use_v3_stores_api, request, api_url, payment_key, division_id, idempotency_key
    ):

        if use_v3_stores_api:
            string_to_sign = ""
            authorization = base64.b64encode(
                f"{division_id}:{payment_key}".encode("ascii")
            ).decode("ascii")
            headers = {"Authorization": f"Basic {authorization}"}
        else:
            if idempotency_key is None:
                idempotency_key = str(uuid.uuid4()) if request.get_idempotence() else ""

            request_method = request.get_method()

            date_header = utils.http_date_now()
            string_to_sign, signature = Middleware.generate_bz_signature(
                payment_key,
                utils.api_url_parse(api_url),
                request_method,
                date_header,
                request.get_path(),
                "",
                idempotency_key,
                json.dumps(request.get_body())
                if request_method in ("POST", "PATCH")
                else "",
            )

            headers = {
                "Date": date_header,
                "Authorization": f"BZ1-HMAC-SHA256 DivisionId={division_id},Signature={signature}",
            }

            if idempotency_key != "":
                headers["Idempotency-Key"] = idempotency_key

        return string_to_sign, headers

    @staticmethod
    def validate_webhook_signature(request, payment_key):
        bz_hook_format = request["Bz-Hook-Format"]
        # stop processing when bz-hook-format = v1 because only v2 requests are implemented
        if bz_hook_format == "v1":
            return False

        body = request["Body"]
        _, signature = Middleware.generate_bz_signature(
            payment_key,
            request["Host"] + ":" + request["Port"],
            request["Method"] if request["Method"] else "POST",
            request["Date"],
            request["Path"],
            "",  # query string
            "",  # idempotency key
            body,
        )
        if request["Bz-Signature"] == f"BZ1-HMAC-SHA256 {signature}":
            return True
        return False
