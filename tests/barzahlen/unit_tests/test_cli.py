import configparser
import os
from pathlib import Path
from unittest import TestCase

from click.testing import CliRunner

from barzahlen.cli import main, generate_signature, config


class CliTestCase(TestCase):
    def test_cli_main(self):
        """Test the CLI."""
        runner = CliRunner()
        help_result = runner.invoke(main, ["-h"])
        self.assertEqual(help_result.exit_code, 0)
        self.assertTrue("Usage" in help_result.output)

    def test_cli_generate_signature(self):
        """Test the CLI."""
        runner = CliRunner()
        result = runner.invoke(
            generate_signature,
            [
                "--api-key",
                "test",
                "--host-header",
                "api-sandbox.barzahlen.de:443",
                "--host-path",
                "/v2/slips",
                "--date-header",
                "Tue, 04 Aug 2020 15:46:05 GMT",
                "--idempotency-key",
                "c66aa364-7fdf-4bc9-9d95-c201cbf4f0d9",
                "--customer-key",
                "customer@example.com",
                "--amount",
                "99.50",
                "--currency",
                "EUR",
            ],
        )

        expected_result = (
            "##############################\n#   Calculate BZ signature   #\n##############################\n"
            "String to sign\n"
            "api-sandbox.barzahlen.de:443\n"
            "POST\n"
            "/v2/slips\n"
            "\n"
            "Tue, 04 Aug 2020 15:46:05 GMT\n"
            "c66aa364-7fdf-4bc9-9d95-c201cbf4f0d9\n"
            "3960a58f4946749559bdffeaf32f8f42063b323b48b9137cc9b07c94b97b5596\n"
            "Signature\n"
            "6e6c42a02ec4e11517e3e1780fe9dc2bcae51b0d12c1bdbfe91ff2518835626e\n"
        )
        self.assertEqual(result.exit_code, 0)
        self.assertEqual(result.output, expected_result)

    def test_cli_config(self):
        """Test the ping operation."""
        runner = CliRunner()
        config_file = f"{str(Path.home())}/.barzahlen_test"
        result = runner.invoke(
            config,
            [
                "--api-key",
                "test",
                "--stores-api-key",
                "test",
                "--division-id",
                "123456",
                "--config-file",
                config_file,
            ],
        )
        self.assertEqual(result.exit_code, 0)
        self.assertTrue(os.path.isfile(config_file))

        conf = configparser.ConfigParser()
        conf.read(config_file)
        self.assertIn("BARZAHLEN", conf)
        self.assertIn("BZ_API_KEY", conf["BARZAHLEN"])
        self.assertIn("BZ_STORES_API_KEY", conf["BARZAHLEN"])
        self.assertIn("BZ_DIVISION_ID", conf["BARZAHLEN"])

        os.remove(config_file)
        self.assertFalse(os.path.isfile(config_file))
