def invalid_slip_type_error():
    return {
        "error_class": "invalid_parameter",
        "error_code": "invalid_slip_type",
        "message": "slip_type: slip_type is required",
        "request_id": "1865359ad354441b9403c3771d811c53",
    }


def invalid_signature_error():
    return {
        "error_class": "auth",
        "error_code": "invalid_signature",
        "message": "authentication error. invalid signature",
        "request_id": "1865359ad354441b9403c3771d811c53",
    }


def generic_transport_error():
    return {
        "error_class": "transport",
        "message": "Generic transport error",
        "request_id": "1865359ad354441b9403c3771d811c53",
    }


def internal_server_error():
    return {
        "error_class": "server_error",
        "error_code": "internal_server_error",
        "message": "500 Internal server error",
        "request_id": "1865359ad354441b9403c3771d811c53",
    }


def unknown_error():
    return {
        "error_class": "not_expected_error",
        "message": "Error",
        "request_id": "1865359ad354441b9403c3771d811c53",
    }


def slip_already_paid_error():
    return {
        "error_class": "invalid_state",
        "error_code": "slip_paid",
        "message": "Error",
        "request_id": "1865359ad354441b9403c3771d811c53",
    }


def slip_not_found_error():
    return {
        "error_class": "invalid_state",
        "error_code": "slip_not_found",
        "message": "Error",
        "request_id": "1865359ad354441b9403c3771d811c53",
    }


def invalid_point_parameters_error():
    return {
        "error_class": "invalid_parameters",
        "error_code": "invalid_point_parameters",
        "message": "The following parameters have invalid values: lat",
        "documentation_url": "https://docs.viafintech.com/api-stores/v3/#errors",
    }


def invalid_bounds_parameters_error():
    return {
        "error_class": "invalid_parameters",
        "error_code": "invalid_bounds_parameters",
        "message": "The following parameters have invalid values: bottom_right_lng",
        "documentation_url": "https://docs.viafintech.com/api-stores/v3/#errors",
    }
