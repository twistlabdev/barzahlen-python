# -*- coding: utf-8 -*-
from copy import deepcopy
import datetime


class Request:
    _idempotence = False
    _path = ""
    _method = ""

    def get_idempotence(self):
        return self._idempotence

    def get_path(self):
        return self._path

    def get_method(self):
        return self._method


class PingRequest(Request):
    _idempotence = False
    _path = "/v2/ping"
    _method = "GET"
    _body = ""


class CreateSlipRequest(Request):
    _idempotence = True
    _path = "/v2/slips"
    _method = "POST"
    _body = None

    def __init__(self, slip_type, customer_key=None):
        self._customer = {}
        self.set_customer_key(customer_key)
        self._slip_type = slip_type
        self._reference_key = None
        self._hook_url = None
        self._expires_at = None
        self._address = {}
        self._transactions = []
        self._metadata = {}
        super().__init__()

    def set_body(self, body):
        self._body = body

    def set_reference_key(self, reference_key):
        self._reference_key = reference_key

    def set_hook_url(self, hook_url):
        """
        Per-slip webhook URL to use instead of the one configured in Control Center
        """
        self._hook_url = hook_url

    def set_expires_at(self, expires_at):
        if isinstance(expires_at, datetime.datetime):
            self._expires_at = expires_at.isoformat()
        else:
            self._expires_at = expires_at

    def set_customer_key(self, customer_key):
        if customer_key is not None:
            self._customer["key"] = customer_key

    def set_customer_cell_phone(self, customer_cell_phone):
        self._customer["cell_phone"] = customer_cell_phone

    def set_customer_email(self, customer_email):
        self._customer["email"] = customer_email

    def set_customer_language(self, customer_language):
        self._customer["language"] = customer_language

    def set_address(self, city, country, street_and_no, zip_code):
        """
        Address is used to show the customer nearby Barzahlen partner stores and to return
        the three nearest stores in API responses. If you provide an address,
        you must provide all four fields.
        """
        self._address = {
            "street_and_no": street_and_no,
            "zipcode": zip_code,
            "city": city,
            "country": country,
        }

    def set_transactions(self, transactions):
        self._transactions = deepcopy(transactions)

    def add_metadata(self, key, value):
        """
        Custom metadata not processed by Barzahlen, returned in responses and webhooks.
        Useful for associating slips with internal identifiers, see also reference_key.
        Maximum 3 keys, max. 15 bytes key length and max. 50 bytes value length.
        Keys and values must be strings.
        """
        self._metadata[key] = value

    def get_body(self):
        if self._body is not None:
            return self._body

        body = {"slip_type": self._slip_type, "transactions": self._transactions}

        if bool(self._customer):  # pragma: no cover
            body["customer"] = self._customer

        if self._reference_key:
            body["reference_key"] = self._reference_key

        if self._hook_url:
            body["hook_url"] = self._hook_url

        if self._expires_at:
            body["expires_at"] = self._expires_at

        if len(self._address) > 0:
            body["show_stores_near"] = {"address": self._address}

        if len(self._metadata) > 0:
            body["metadata"] = self._metadata

        return body


class InvalidateSlipRequest(Request):
    _idempotence = False
    _method = "POST"
    _body = ""

    def __init__(self, slip_id):
        self._path = f"/v2/slips/{slip_id}/invalidate"
        super().__init__()

    def get_body(self):
        return self._body


class RetrieveSlipRequest(Request):
    _idempotence = False
    _method = "GET"
    _body = ""

    def __init__(self, slip_id):
        self._path = f"/v2/slips/{slip_id}"
        super().__init__()


class UpdateSlipRequest(Request):
    _idempotence = False
    _method = "PATCH"
    _body = {}

    def __init__(self, slip_id):
        self._path = f"/v2/slips/{slip_id}"
        self._slip_id = slip_id
        self._customer = {}
        self._expires_at = None
        self._reference_key = None
        self._transactions = []
        super().__init__()

    def set_customer_email(self, customer_email):
        self._customer["email"] = customer_email

    def set_expires_at(self, expires_at):
        if isinstance(expires_at, datetime.datetime):
            self._expires_at = expires_at.isoformat()
        else:
            self._expires_at = expires_at

    def set_customer_cell_phone(self, customer_cell_phone):
        self._customer["cell_phone"] = customer_cell_phone

    def set_reference_key(self, reference_key):
        self._reference_key = reference_key

    def set_transactions(self, transactions):
        self._transactions = deepcopy(transactions)

    def get_body(self):

        if bool(self._customer):  # pragma: no cover
            self._body["customer"] = self._customer

        if self._reference_key:  # pragma: no cover
            self._body["reference_key"] = self._reference_key

        if self._expires_at:  # pragma: no cover
            self._body["expires_at"] = self._expires_at

        if len(self._transactions) > 0:  # pragma: no cover
            self._body["transactions"] = self._transactions

        return self._body


class StoresNearbyRequest(Request):
    _idempotence = False
    _method = "GET"
    _body = ""

    def __init__(self, lat, lng):
        self._path = f"/v3/stores/nearby?lat={lat}&lng={lng}"
        super().__init__()


class StoresWithinBoundsRequest(Request):
    _idempotence = False
    _method = "GET"
    _body = ""

    def __init__(self, top_left_lat, top_left_lng, bottom_right_lat, bottom_right_lng):
        self._path = (
            f"/v3/stores/within_bounds?"
            f"top_left_lat={top_left_lat}"
            f"&top_left_lng={top_left_lng}"
            f"&bottom_right_lat={bottom_right_lat}"
            f"&bottom_right_lng={bottom_right_lng}"
        )
        super().__init__()
