# -*- coding: utf-8 -*-
from setuptools import setup, find_packages
from barzahlen import __version__

version = __version__
readme = open('README.md').read()
history = open('CHANGELOG.md').read()

with open('requirements/base.txt') as fp:
    install_requires = fp.read().splitlines()
with open('requirements/tests.txt') as fp:
    test_install_requires = fp.read().splitlines()[1:]

setup(
    author='Twist Lab',
    author_email='dev@twistlab.ch',
    python_requires='>=3.6',
    classifiers=[
        'Development Status :: Beta',
        'Intended Audience :: Developers',
        'License :: No License',
        'Natural Language :: English',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Topic :: Software Development :: Libraries',
    ],
    description="Barzahlen API Client in Python",
    entry_points={
        'console_scripts': [
            'barzahlen_python=barzahlen.cli:main',
        ],
    },
    install_requires=install_requires,
    license="MIT license",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='barzahlen_python',
    name='barzahlen_python',
    packages=find_packages(include=['barzahlen', 'barzahlen.*']),
    test_suite='tests',
    tests_require=test_install_requires,
    url='https://gitlab.com/twistlabdev/barzahlen-python',
    version=__version__,
    zip_safe=False,
)
