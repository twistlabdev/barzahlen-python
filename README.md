# Twist Lab - Barzahlen Python

This project provides a Python client to the [Barzahlen API v2][api_documentation_base].

Additionally this SDK implementes the new [viafintech API Stores v3][api_documentation_stores_api] which provides two endpoints to retrieve stores which offer the Barzahlen/viacash service including their opening hours and coordinates.

## Installation

## API Endpoints implemented

*Barzhalen API v2*

| Feature      | Endpoint | Documentation section     |
| :---        |    :---   |          :--- |
| Ping    |  GET /v2/ping      |  [Ping endpoint][api_documentation_ping]  |
| Create payment slip   |  GET /v2/slips      |  [Create slip endpoint][api_documentation_slip]  |
| Invalidate slip    |  POST /v2/slips/{id}/invalidate      |  [Invalidate slip endpoint][api_documentation_invalidate]  |
| Retrieve slip    |  GET /v2/slips/{id}      |  [Retrieve slip endpoint][api_documentation_retrieve]  |
| Update slip    |  PATCH /v2/slips/{id}      |  [Update slip endpoint][api_documentation_update]  |

*viafintech API Stores v3*

| Feature      | Endpoint | Documentation section     |
| :---        |    :---   |          :--- |
| Stores nearby    |  GET /v3/stores/nearby      |  [Stores nearby endpoint][api_documentation_stores_nearby]  |
| Stores within bounds   |  GET /v3/stores/within_bounds      |  [Stores within bounds endpoint][api_documentation_stores_within_bounds]  |


## Usage

### ping
Basic usage connecting and running a ping request:

```python
client = BarzahlenAPIClient(division_id, api_key, sandbox=False)
ping_response = client.ping()
```

### Create payment (deposit or withdrawal)

Example usage of creating a payment (deposit) or payout(withdrawal):

```python
client = BarzahlenAPIClient(division_id, api_key, sandbox=False)
try:
    slip_response = client.create_payment(customer_key, float(amount), currency, email=email)
except (error.BarzahlenAPIClientError, error.ApiError) as err:
    print('There was an error creating a slip')
```

Additional parameters:

Extra named parameters:

- cell_phone: The customer's cell phone number in international format.
    For payment slips, triggers a text message being sent if provided.
- email: The customer's email address. Triggers an email being sent if provided.
- language: The language used for communicating with the customer
    One of: null or "de-DE" or "de-CH" or "en-CH" or "it-IT"
- expires_at: Time the slip expires and can no longer be used. Will be set automatically if not provided.
    Can be a datetime object or a string in ISO RFC 3339 format
- hook_url: Per-slip webhook URL to use instead of the one configured in Control Center.
- metadata: Custom metadata not processed by Barzahlen, returned in responses and webhooks.
    Useful for associating slips with internal identifiers. Maximum of 3 keys.
    Example:  {'my_internal_key': 'my_internal_value'}
- reference_key: A key referencing an order or another internal process.
- Address (not a dict key): Following 4 fields must be provided all together.
    - street_and_no: The customer's street and house number for showing nearby stores.
    - zipcode: The customer's zip code for showing nearby stores.
    - country: The customer's country for showing nearby stores
    - city: The customer's city for showing nearby stores

More information about parameters format and patterns on [Request Attributes][api_documentation_slip]


### Invalidate slip (deposit or withdrawal)

Example usage of invalidating a sliip:

```python
client = BarzahlenAPIClient(division_id, api_key, sandbox=False)
try:
    slip_response = client.invalidate_slip(slip_id)
except (error.BarzahlenAPIClientError, error.ApiError) as err:
    print('There was an error invalidating a slip')
```

#### Idempotency (retry-after feature)

From [Idempotency][api_documentation_idempotency] docs:

> Most methods on resources in Barzahlen API v2 can or even must be used in an idempotent way, so you can automatically retry requests without having > > > unintended side effects.
Retrying requests makes your system more resilient when network failures happen, our system temporarily fails to process your request or your division exceeds the rate limit. We highly recommend implementing automated retries - they are necessary for reliable use of any network-based API.

> When retrying the same request with the same key, the request will be ignored by the API. Instead of creating a new slip, it returns the slip's current state. Never reuse an idempotency key, we recommand using a randomly generated string, e.g. a UUID v4.

The `create_payment` method automatically generates a UUID v4 key for each request if the `idempotency_key` parameter is not provided.
It is the responsability of the caller library to generate and save a proper UUID v4 and pass it to the `create_payment` method so the operation can be retried if the result is not successful. Otherwise, the Barzahlen SDK will just simply generate a new one on each request.


## CLI

A command line interpreter is available for testing and debugging purposes.

You can run the CLI after installing the package on your system just directly by typing::

```bash
$ barzahlen_python
Usage: barzahlen_python [OPTIONS] COMMAND [ARGS]...

Options:
  --version   Show the version and exit.
  -h, --help  Show this message and exit.

Commands:
  config                Stores API credentials for subsequent commands
  create-slip           Create a payment, partial payment, payout or refund slip
  generate-signature    Test internal algorithm to generate Barzahlen API signature
  invalidate-slip       Invalidate a slip
  ping                  Check service availability and authentication headers calculation
  retrieve-slip         Retrieve a slip
  stores-nearby         Get nearby stores from lat/lng coordinates
  stores-within-bounds  Get stores within bounds
  update-slip           Update a slip
```

or using the Python interpreter:

```bash
$ python -m barzahlen.cli
Usage: barzahlen_python [OPTIONS] COMMAND [ARGS]...

Options:
  --version   Show the version and exit.
  -h, --help  Show this message and exit.

Commands:
  config                Stores API credentials for subsequent commands
  create-slip           Create a payment, partial payment, payout or refund slip
  generate-signature    Test internal algorithm to generate Barzahlen API signature
  invalidate-slip       Invalidate a slip
  ping                  Check service availability and authentication headers calculation
  retrieve-slip         Retrieve a slip
  stores-nearby         Get nearby stores from lat/lng coordinates
  stores-within-bounds  Get stores within bounds
  update-slip           Update a slip
```

Implemented commands:

### config
Prompts for the API_KEY and Division ID and stores them in a INI file under the user's home directory, usually (~/.barzahlen)
This helps the usage of the following commands by loading this information from the config file.

```bash
$ barzahlen_python config
Please enter your Barzahlen API key: aaaaaaaa
Please enter your Division Id: 1234
##############################
#    Save API credentials    #
##############################
Credentials saved to: /home/luis/.barzahlen
```

### ping
Chech whether the Barzahlen API is up and running.
Any API_KEY and Division_id can be used as this service does not require a valid subscription.

```bash
$ barzahlen_python ping
API key [aaaaaaaa]:
Division Id [1234]:
##############################
#        Ping service        #
##############################
URL
https://api-sandbox.barzahlen.de/v2/ping
String to sign
api-sandbox.barzahlen.de:443
GET
/v2/ping

Fri, 07 Aug 2020 08:42:52 GMT
12ae32cb1ec02d01eda3581b127c1fee3b0dc53572ed6baf239721a03d82e126
Headers
{
    "Date": "Fri, 07 Aug 2020 08:42:52 GMT",
    "Authorization": "BZ1-HMAC-SHA256 DivisionId=1234,Signature=1fcf2780db153d14204ddae07177321f5575f1d8c0971c016ec330dee52e49cd"
}
Response status code
200
Response content
{}
Service UP. Authentication successful
```

### generate-signature
This command test the internal algorithm to generate the signature used in the Authorization header required by the Barzahlen API v2.
More information and a signature calculator can be found in the [Signature generator ][api_documentation_signature]

```bash
$ barzahlen_python generate-signature
API key [aaaaaaaa]:
Host:port [api-sandbox.barzahlen.de:443]:
Endpoint [/v2/slips]:
Date header [Fri, 07 Aug 2020 08:47:02 GMT]:
Idempotency key [9802048b-e6e8-4a90-ae59-746fa5ced115]:
Customer key [customer@example.com]:
Payment amount [99.50]:
Payment currency [EUR]:
##############################
#   Calculate BZ signature   #
##############################
String to sign
api-sandbox.barzahlen.de:443
POST
/v2/slips

Fri, 07 Aug 2020 08:47:02 GMT
9802048b-e6e8-4a90-ae59-746fa5ced115
3960a58f4946749559bdffeaf32f8f42063b323b48b9137cc9b07c94b97b5596
Signature
ade64ed90b17d7b8e3656b51432a9c00342005a30c4dd768461ce40d7e84158a
```

### create-slip
Creates a slip. At the moment only 'payment' slips are available as slip-type.

Parameters:

```bash
barzahlen_python create-slip -h
Usage: barzahlen_python create-slip [OPTIONS]

  Create a payment, partial payment, payout or refund slip

Options:
  --api-key TEXT
  --division-id TEXT
  -t, --slip-type TEXT     [required]
  -k, --customer-key TEXT  [required]
  -a, --amount TEXT        [required]
  -c, --currency TEXT
  -e, --email TEXT
  -h, --help               Show this message and exit.
```

Example:

```bash
$ barzahlen_python create-slip --slip-type payment --customer-key customer@example.com --amount 90.0 --currency EU
```

### invalidate-slip
Invalidates a slip in pending state.

Parameters:

```bash
barzahlen_python invalidate-slip -h
Usage: barzahlen_python invalidate-slip [OPTIONS]

  Invalidate a slip

Options:
  --api-key TEXT
  --division-id TEXT
  -s, --slip-id TEXT  [required]
  -h, --help          Show this message and exit.
```

Example:

```bash
$ barzahlen_python invalidate-slip --slip-id "slp-738c5997-fa25-4e5c-8665-eaed0e91d45f"
```


### update-slip
Update a slip.

Parameters:

```bash
barzahlen_python update-slip -h
Usage: barzahlen_python update-slip [OPTIONS]

  Update a slip

Options:
  --api-key TEXT
  --division-id TEXT
  -s, --slip-id TEXT        [required]
  -a, --amount TEXT
  -c, --cell-phone TEXT
  -e, --email TEXT
  -r, --reference-key TEXT
  -h, --help                Show this message and exit.
```

Example:

```bash
$ barzahlen_python update-slip --slip-id "slp-738c5997-fa25-4e5c-8665-eaed0e91d45f" --amount 50.0
```


### stores-nearby
Get nearby stores from lat/lng coordinates

Parameters:



```bash
python barzahlen_python stores-nearby -h
Usage: cli.py stores-nearby [OPTIONS]

  Get nearby stores from lat/lng coordinates

Options:
  --api-key TEXT
  --stores-api-key TEXT
  --division-id TEXT
  -l, --lat TEXT         [required]
  -g, --lng TEXT
  -h, --help             Show this message and exit.
```

Example:

```bash
$ barzahlen_python stores-nearby --lat 46.552746575072426 --lng 6.600405404779666
```


### stores-within-bounds
Get stores within bounds of the provided coordinates describing a rectangle (upper left and bottom right points).

Parameters:

```bash
python barzahlen_python stores-within-bounds -h
Usage: cli.py stores-within-bounds [OPTIONS]

  Get stores within bounds

Options:
  --api-key TEXT
  --stores-api-key TEXT
  --division-id TEXT
  -t, --top-left-lat TEXT      [required]
  -l, --top-left-lng TEXT      [required]
  -b, --bottom-right-lat TEXT  [required]
  -r, --bottom-right-lng TEXT  [required]
  -h, --help                   Show this message and exit.
```

Example:

```bash
$ barzahlen_python stores-within-bounds -t 46.552746575072426 -l 6.600405404779666 -b 46.51637469033725 -r 6.664097368195876
```

## Dependencies

- requests 2.23.0


## Running tests

Create a virtual env:

    python3 -m venv ~/.venvs/barzhalen_python

Activate it:

    source ~/.venvs/barzhalen_python/bin/activate

Install tox:

    pip install tox

To run all test simply run:

    tox

Tests can also be run in your current env without tox:

    pip install -r requirements/tests.txt
    python -m pytest


## Code coverage

The coverage is automatically reported within tox. To run it manually:

    pip install -r requirements/tests.txt
    pip install coverage
    coverage run -m pytest
    coverage report

Html report can be obtained with (it will be stored in `htmlcov` folder):

    coverage html

Alternatively you can use tox with:

    tox -e coverage

## Python version

Compatible versions:
* Python 3.6
* Python 3.7
* Python 3.8

## TODO

* Create payout type of slips (withdrawal)
* Retrieve and update slip
* Retrieve slip PDF
* Resend email / text message
* Implement and enforce rate limiting


---
(c) Twist Lab Sàrl | www.twistlab.ch

[control_center_app]: https://controlcenter.barzahlen.de
[api_documentation_base]: http://docs.barzahlen.de/api/v2
[api_documentation_idempotency]: http://docs.barzahlen.de/api/v2#idempotency
[api_documentation_signature]: http://docs.barzahlen.de/api/v2#calculating-the-signature
[api_documentation_webhooks]: http://docs.barzahlen.de/api/v2#webhooks
[api_documentation_rate_limit]: http://docs.barzahlen.de/api/v2#rate-limiting
[api_documentation_sandbox]: http://docs.barzahlen.de/api/v2#sandbox
[api_documentation_slip]: http://docs.barzahlen.de/api/v2#create-slip
[api_documentation_ping]: http://docs.barzahlen.de/api/v2#ping
[api_documentation_retrieve]: http://docs.barzahlen.de/api/v2#retrieve-slip
[api_documentation_update]: http://docs.barzahlen.de/api/v2#update-slip
[api_documentation_resend]: http://docs.barzahlen.de/api/v2#resend-email-text-message
[api_documentation_invalidate]: http://docs.barzahlen.de/api/v2#invalidate-slip
[api_documentation_error]: http://docs.barzahlen.de/api/v2#errors
[api_documentation_stores_api]: https://docs.viafintech.com/api-stores/v3/#introduction
[api_documentation_stores_nearby]: https://docs.viafintech.com/api-stores/v3/#stores-nearby
[api_documentation_stores_within_bounds]: https://docs.viafintech.com/api-stores/v3/#stores-within_bounds

