from unittest import TestCase

import responses

from barzahlen import error
from barzahlen.client import BarzahlenAPIClient
from tests.fixtures.barzhalen_responses import retrieve_invalidated_slip_response

# from tests.fixtures.barzahlen_errors import slip_already_paid_error


class ApiBarzahlenAPIInvaliadateSlipsTestCase(TestCase):
    def test_retrieve_slip_invalid_parameters(self):
        client = BarzahlenAPIClient("test", "test", sandbox=True)
        with self.assertRaises(error.IncorrectApiParametersError):
            client.retrieve_slip(None)

    @responses.activate
    def test_retrieve_slip_ok(self):
        slip_id = "slp-31ab7b60-3df8-4979-8045-8b1af0bee895"
        response_body = retrieve_invalidated_slip_response()
        responses.add(
            responses.GET,
            f"https://api-sandbox.barzahlen.de/v2/slips/{slip_id}",
            json=response_body,
            status=200,
        )

        client = BarzahlenAPIClient("test", "test", sandbox=True)
        retrieve_response = client.retrieve_slip(slip_id)

        self.assertEqual(retrieve_response.get_response_status_code(), 200)
