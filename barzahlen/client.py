# -*- coding: utf-8 -*-
import requests

from . import error
from .config import API_HOST, API_HOST_SANDBOX, STORES_API_HOST, DEFAULT_TIMEOUT
from .middleware import Middleware
from .request import (
    PingRequest,
    CreateSlipRequest,
    InvalidateSlipRequest,
    RetrieveSlipRequest,
    UpdateSlipRequest,
    StoresNearbyRequest,
    StoresWithinBoundsRequest,
)
from .response import (
    BarzahlenResponse,
    CreateSlipResponse,
    InvalidateSlipResponse,
    RetrieveSlipResponse,
    UpdateSlipResponse,
    StoresNearbyResponse,
    StoresWithinBoundsResponse,
)


class BarzahlenAPIClient:
    def __init__(
        self,
        division_id,
        payment_key,
        stores_api_key=None,
        sandbox=False,
        timeout=DEFAULT_TIMEOUT,
        proxy_url=None,
        proxy_port=None,
    ):
        if division_id is None or payment_key is None:
            raise error.IncorrectApiParametersError(
                message="Division Id and Payment Key are mandatory fields"
            )
        self._proxies = None
        self._division_id = division_id
        self._payment_key = payment_key
        self._stores_api_key = stores_api_key
        self._api_url = API_HOST_SANDBOX if sandbox else API_HOST
        self._stores_api_url = STORES_API_HOST
        self._timeout = timeout
        self._current_protocol = []

        if proxy_url and proxy_port:
            self._proxies = {
                "http": f"http://{proxy_url}:{proxy_port}",
                "https": f"https://{proxy_url}:{proxy_port}",
            }

    def get_api_url(self):
        return self._api_url

    def get_stores_api_url(self):
        return self._stores_api_url

    def get_proxies(self):
        return self._proxies

    def _get(self, api_request, use_v3_stores_api=False):
        if use_v3_stores_api and self._stores_api_key is None:
            raise error.IncorrectApiParametersError(
                message="Barzahlen client initialized with no Stores API key"
            )
        base_url = str(self._stores_api_url if use_v3_stores_api else self._api_url)
        url = f"{base_url}{api_request.get_path()}"
        string_to_sign, headers = Middleware.build_headers(
            use_v3_stores_api,
            api_request,
            self._stores_api_url if use_v3_stores_api else self._api_url,
            self._stores_api_key if use_v3_stores_api else self._payment_key,
            self._division_id,
            None,
        )
        # noinspection PyUnusedLocal
        try:
            response = requests.get(
                url, headers=headers, timeout=self._timeout, proxies=self._proxies
            )
            response.raise_for_status()
        except requests.exceptions.HTTPError:
            # requests lib raises an exception for result codes between 400 and 600
            # we catch this exception just to stop propagation
            # when response is returned, these codes will be handled by the BarzahlenResponseValidator
            # and will raise a custom exception based on the response body attributes
            pass
        except requests.exceptions.RequestException as err:
            raise error.BarzahlenNetworkError() from err
        return url, string_to_sign, headers, response

    def _post(self, api_request, idempotency_key=None, data=None, usePatchMethod=False):
        url = f"{self._api_url}{api_request.get_path()}"
        string_to_sign, headers = Middleware.build_headers(
            False,
            api_request,
            self._api_url,
            self._payment_key,
            self._division_id,
            idempotency_key,
        )
        # noinspection PyUnusedLocal
        try:
            if usePatchMethod:
                response = requests.patch(
                    url,
                    json=data,
                    headers=headers,
                    timeout=self._timeout,
                    proxies=self._proxies,
                )
            else:
                response = requests.post(
                    url,
                    json=data,
                    headers=headers,
                    timeout=self._timeout,
                    proxies=self._proxies,
                )
            response.raise_for_status()
        except requests.exceptions.HTTPError:
            # requests lib raises an exception for result codes between 400 and 600
            # we catch this exception just to stop propagation
            # when response is returned, these codes will be handled by the BarzahlenResponseValidator
            # and will raise a custom exception based on the response body attributes
            pass
        except requests.exceptions.RequestException as err:
            raise error.BarzahlenNetworkError() from err
        return url, string_to_sign, headers, response

    def _create_slip(
        self,
        slip_type=None,
        customer_key=None,
        transactions=None,
        idempotency_key=None,
        **kwargs,
    ):
        """
        Create a payment, partial payments, payout, or refund slip.

        - Required fields for all slip types:
            · slip_type
            · transactions/currency, transactions/amount

        - All payment, payout and refund slips must contain exactly one transaction.
        - Partial payments must contain at least two and at most twelve transactions.
        - If an email address is provided, an email with a PDF attachment will be
          sent to the client containing the barcode

        More information on Barzahlen docs:
            https://docs.barzahlen.de/api/v2/#create-slip
        """
        if customer_key is None or slip_type is None:
            raise error.IncorrectApiParametersError(
                "Customer key and slip type are mandatory fields"
            )

        create_request = CreateSlipRequest(slip_type, customer_key)
        create_request.set_transactions(transactions)

        address_fields = {"street_and_no", "zipcode", "city", "country"}

        if "email" in kwargs:
            create_request.set_customer_email(kwargs.pop("email"))
        if "cell_phone" in kwargs:
            create_request.set_customer_cell_phone(kwargs.pop("cell_phone"))
        if "language" in kwargs:
            create_request.set_customer_language(kwargs.pop("language"))
        if "expires_at" in kwargs:
            create_request.set_expires_at(kwargs.pop("expires_at"))
        if "reference_key" in kwargs:
            create_request.set_reference_key(kwargs.pop("reference_key"))
        if "hook_url" in kwargs:
            create_request.set_hook_url(kwargs.pop("hook_url"))
        if all(k in kwargs for k in address_fields):
            create_request.set_address(
                kwargs.pop("city"),
                kwargs.pop("country"),
                kwargs.pop("street_and_no"),
                kwargs.pop("zipcode"),
            )

        if "metadata" in kwargs:
            for key, value in kwargs.pop("metadata").items():
                create_request.add_metadata(key, value)

        return CreateSlipResponse(
            *self._post(create_request, idempotency_key, data=create_request.get_body())
        )

    def ping(self):
        """
        Check if service is available.

        The easiest method for checking whether the Barzhalen
        service is available.

        More information on Barzahlen docs:
            https://docs.barzahlen.de/api/v2/#ping
        """
        ping_request = PingRequest()
        return BarzahlenResponse(*self._get(ping_request))

    def create_payment(
        self, customer_key, amount, currency, idempotency_key=None, **kwargs
    ):
        """
        Create payment (deposit)

        This is the minimal type of payment slip request.
        Only includes a single transaction amount/currency

        Extra named parameters:

            - cell_phone: The customer's cell phone number in international format.
              For payment slips, triggers a text message being sent if provided.
            - email: The customer's email address. Triggers an email being sent if provided.
            - language: The language used for communicating with the customer
                One of: null or "de-DE" or "de-CH" or "en-CH" or "it-IT"
            - expires_at: Time the slip expires and can no longer be used. Will be set automatically if not provided.
                Can be a datetime object or a string in ISO RFC 3339 format
            - hook_url: Per-slip webhook URL to use instead of the one configured in Control Center.
            - metadata: Custom metadata not processed by Barzahlen, returned in responses and webhooks.
                Useful for associating slips with internal identifiers. Maximum of 3 keys.
            - reference_key: A key referencing an order or another internal process.
            - Address ( not a key): Following 4 fields must be provided all together.
                - street_and_no: The customer's street and house number for showing nearby stores.
                - zipcode: The customer's zip code for showing nearby stores.
                - country: The customer's country for showing nearby stores
                - city: The customer's city for showing nearby stores

        """
        if amount is None:
            raise error.IncorrectApiParametersError("Amount value cannot be null")
        if not isinstance(amount, float):
            raise error.IncorrectApiParametersError(
                "Amount value must be a float number"
            )

        if amount <= 0:
            raise error.IncorrectApiParametersError(
                "Amount value must be positive for payment slips"
            )

        return self._create_slip(
            "payment",
            customer_key,
            [
                {
                    "amount": str(amount),
                    "currency": currency if currency is not None else "EUR",
                }
            ],
            idempotency_key,
            **kwargs,
        )

    def create_payout(
        self, customer_key, amount, currency, idempotency_key=None, **kwargs
    ):
        """
        Create payout (withdrawal)

        Same parameters a in create_payment. Amount must be negative.

        """
        if amount is None:
            raise error.IncorrectApiParametersError("Amount value cannot be null")
        if not isinstance(amount, float):
            raise error.IncorrectApiParametersError(
                "Amount value must be a float number"
            )

        if amount >= 0:
            raise error.IncorrectApiParametersError(
                "Amount value must be negative por payout slips"
            )

        return self._create_slip(
            "payout",
            customer_key,
            [
                {
                    "amount": str(amount),
                    "currency": currency if currency is not None else "EUR",
                }
            ],
            idempotency_key,
            **kwargs,
        )

    def retrieve_slip(self, slip_id):
        """
        Retrieve information about a slip.

        Response attributes documented on https://docs.barzahlen.de/api/v2/#retrieve-slip
        """
        if slip_id is None:
            raise error.IncorrectApiParametersError("Slip id is a mandatory field")

        retrieve_request = RetrieveSlipRequest(slip_id)
        return RetrieveSlipResponse(*self._get(retrieve_request))

    def invalidate_slip(self, slip_id):
        """
        Invalidate a slip so it can no longer be used.
        Only possible for slips with a pending transaction, so e.g. paid slips
        can not be invalidated. For partial payment slips it is possible to invalidate
        all the still pending transactions.

        This action is final and can not be undone. An invalid slip can no longer be updated or
        resent via the API or paid by the customer.

        When the slip contains a customer:email, we notify the customer via email that the slip
        is no longer valid.

        Invalidating an already invalidated slip does nothing and returns the slip as if it had
        not been invalidated before.
        """
        if slip_id is None:
            raise error.IncorrectApiParametersError("Slip id is a mandatory field")

        invalidate_request = InvalidateSlipRequest(slip_id)
        return InvalidateSlipResponse(
            *self._post(invalidate_request, data=invalidate_request.get_body())
        )

    def update_slip(
        self,
        slip_id,
        amount=None,
        reference_key=None,
        customer_email=None,
        customer_cell_phone=None,
        expires_at=None,
    ):
        """
        Change certain slip attributes. Only slips with a pending transaction can be updated.
        We notify the customer about certain changes.

        IMPORTANT:  Only one-transaction payments/payouts are supported so far.

        Common use case is for updating the transaction amount.

        For payments, the following attributes can be changed:

        - customer:cell_phone if the text message resend limit has not been exceeded, see Resend Email / Text Message
        - customer:email
        - expires_at
        - reference_key if it is not yet set
        - transactions/amount

        For payouts, the following attributes can be changed:

        - expires_at
        - reference_key if it is not yet set

        Response attributes documented on https://docs.barzahlen.de/api/v2/#update-slip
        """
        if slip_id is None:
            raise error.IncorrectApiParametersError("Slip id is a mandatory field")

        if all(
            v is None
            for v in [
                amount,
                reference_key,
                customer_email,
                customer_cell_phone,
                expires_at,
            ]
        ):
            raise error.IncorrectApiParametersError(
                "At least one field must be provided to be updated"
            )

        slip_response = self.retrieve_slip(slip_id).get_response_content_as_json()

        update_request = UpdateSlipRequest(slip_id)
        update_request.set_customer_cell_phone(customer_cell_phone)
        update_request.set_customer_email(customer_email)

        if amount is not None:
            if not isinstance(amount, float):
                raise error.IncorrectApiParametersError(
                    "Amount value must be a float number"
                )

            transaction_id = slip_response["transactions"][0]["id"]
            update_request.set_transactions(
                [
                    {
                        "id": transaction_id,
                        "amount": str(amount),
                    }
                ]
            )

        update_request.set_expires_at(expires_at)
        update_request.set_reference_key(reference_key)

        return UpdateSlipResponse(
            *self._post(
                update_request, data=update_request.get_body(), usePatchMethod=True
            )
        )

    def get_nearby_stores(self, lat, lng):
        """
        Nearby returns a list of the 10 nearest stores to the given point

        Response example on https://docs.viafintech.com/api-stores/v3/#stores-nearby
        """
        if lat is None or lng is None:
            raise error.IncorrectApiParametersError(
                "Latitude and longitude are mandatory parameters"
            )

        stores_request = StoresNearbyRequest(lat, lng)
        return StoresNearbyResponse(*self._get(stores_request, use_v3_stores_api=True))

    def get_stores_within_bounds(
        self, top_left_lat, top_left_lng, bottom_right_lat, bottom_right_lng
    ):
        """
        Within Bounds returns a list of stores contained within the formed bounding box of 2 points (top_left, bottom_right)

        Response example on https://docs.viafintech.com/api-stores/v3/#stores-within_bounds
        """
        if any(
            v is None
            for v in [top_left_lat, top_left_lng, bottom_right_lat, bottom_right_lng]
        ):
            raise error.IncorrectApiParametersError(
                "Bounding box coordinates are mandatory"
            )

        stores_request = StoresWithinBoundsRequest(
            top_left_lat, top_left_lng, bottom_right_lat, bottom_right_lng
        )
        return StoresWithinBoundsResponse(
            *self._get(stores_request, use_v3_stores_api=True)
        )

    # def create_partial_payments(self, customer_key, transactions, **kwargs):
    #     """
    #     Create payment

    #     Partial Payment slips can be paid by a customer and result in money being
    #     transferred to your division for each individual payment.
    #     They are used e.g. when a customer pays their monthly electricity bill.
    #     At least two and at most twelve transactions may be provided.

    #     Parameters:

    #         - transactions: list of transactions. "displayed_due_at" is mandatory for partial_payments.
    #           Example:
    #             "transactions": [
    #                 { "currency": "EUR", "amount": "123.34", "displayed_due_at": "2020-05-31T22:00:00Z" },
    #                 { "currency": "EUR", "amount": "123.34", "displayed_due_at": "2020-06-30T22:00:00Z" }
    #             ]

    #     Extra named parameters: Same as in create_payment

    #     """
    #     return self._create_slip('partial_payments', customer_key, transactions, **kwargs)
