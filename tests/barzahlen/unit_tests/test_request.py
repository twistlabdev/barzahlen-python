from unittest import TestCase

from barzahlen.request import CreateSlipRequest, UpdateSlipRequest


class RequestTestCase(TestCase):
    def test_request_set_body(self):
        request = CreateSlipRequest("payment", "customer@customer.com")
        body = {
            "slip_type": "payment",
            "customer": {"key": "LDFKHSLFDHFL"},
            "transactions": [{"currency": "EUR", "amount": "123.34"}],
        }
        request.set_body(body)
        self.assertDictEqual(body, request.get_body())

    def test_request_customer_key_none(self):
        request = CreateSlipRequest("payment", None)
        body = {
            "slip_type": "payment",
            "transactions": [{"currency": "EUR", "amount": "123.34"}],
        }
        request.set_body(body)
        self.assertDictEqual(body, request.get_body())

    def test_update_request(self):
        request = UpdateSlipRequest("slip-id")
        body = {
            "customer": {"email": "john@example.com", "cell_phone": "+495423112345"},
            "expires_at": "2016-01-10T12:34:56Z",
            "transactions": [{"id": "4729294329", "amount": "150.00"}],
            "reference_key": "NEWKEY",
        }

        request.set_customer_email("john@example.com")
        request.set_customer_cell_phone("+495423112345")
        request.set_reference_key("NEWKEY")
        request.set_expires_at("2016-01-10T12:34:56Z")
        request.set_transactions([{"id": "4729294329", "amount": "150.00"}])

        self.assertDictEqual(body, request.get_body())
        request = None
