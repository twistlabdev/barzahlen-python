# -*- coding: utf-8 -*-

API_HOST = "https://api.barzahlen.de"
API_HOST_SANDBOX = "https://api-sandbox.barzahlen.de"
STORES_API_HOST = "https://stores.viafintech.com"
CLI_CONFIG_FILE = ".barzahlen"
DEFAULT_TIMEOUT = 10.0
