from unittest import TestCase

import responses

from barzahlen import error
from barzahlen.client import BarzahlenAPIClient
from tests.fixtures.barzhalen_responses import invalidated_slip_response
from tests.fixtures.barzahlen_errors import slip_already_paid_error


class ApiBarzahlenAPIInvaliadateSlipsTestCase(TestCase):
    def test_invalidate_slip_invalid_parameters(self):
        client = BarzahlenAPIClient("test", "test", sandbox=True)
        with self.assertRaises(error.IncorrectApiParametersError):
            client.invalidate_slip(None)

    @responses.activate
    def test_invalidate_slip_ok(self):
        slip_id = "slp-738c5997-fa25-4e5c-8665-eaed0e91d45f"
        response_body = invalidated_slip_response()
        responses.add(
            responses.POST,
            f"https://api-sandbox.barzahlen.de/v2/slips/{slip_id}/invalidate",
            json=response_body,
            status=200,
        )

        client = BarzahlenAPIClient("test", "test", sandbox=True)
        invalidate_response = client.invalidate_slip(slip_id)

        self.assertEqual(invalidate_response.get_response_status_code(), 200)

    @responses.activate
    def test_payment_already_paid(self):
        slip_id = "slp-59c4d390-0fbf-4c48-a659-6a3707164872"
        response_body = slip_already_paid_error()
        responses.add(
            responses.POST,
            f"https://api-sandbox.barzahlen.de/v2/slips/{slip_id}/invalidate",
            json=response_body,
            status=400,
        )

        client = BarzahlenAPIClient("test", "test", sandbox=True)

        with self.assertRaises(error.SlipAlreadyPaidError):
            invalidate_response = client.invalidate_slip(slip_id)
            self.assertEqual(invalidate_response.get_response_status_code(), 400)
