from unittest import TestCase

from barzahlen.middleware import Middleware
from tests.fixtures.barzahlen_webhooks import transaction_paid_webhook_request


class ApiBarzahlenAPIWebhookTestCase(TestCase):
    def test_webhook_valid_signature(self):

        test_payment_key = "6b3fb3abef828c7d10b5a905a49c988105621395"
        webhook_request = transaction_paid_webhook_request()
        valid_signature = Middleware.validate_webhook_signature(
            webhook_request, test_payment_key
        )
        self.assertTrue(valid_signature)

    def test_webhook_v1_not_supported(self):

        test_payment_key = "6b3fb3abef828c7d10b5a905a49c988105621395"
        webhook_request = transaction_paid_webhook_request()
        webhook_request["Bz-Hook-Format"] = "v1"
        valid_signature = Middleware.validate_webhook_signature(
            webhook_request, test_payment_key
        )
        self.assertFalse(valid_signature)

    def test_webhook_invalid_signature(self):

        wrong_test_payment_key = "7b3fb3abef828c7d10b5a905a49c988105621395"
        webhook_request = transaction_paid_webhook_request()
        valid_signature = Middleware.validate_webhook_signature(
            webhook_request, wrong_test_payment_key
        )
        self.assertFalse(valid_signature)
