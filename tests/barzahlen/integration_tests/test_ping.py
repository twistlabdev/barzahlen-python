from unittest import TestCase

import responses

from barzahlen.client import BarzahlenAPIClient


class ApiBarzahlenAPIPingTestCase(TestCase):
    @responses.activate
    def test_ping(self):
        responses.add(
            responses.GET,
            "https://api-sandbox.barzahlen.de/v2/ping",
            json={},
            status=200,
        )
        client = BarzahlenAPIClient("test", "test", sandbox=True)
        ping_response = client.ping()
        self.assertEqual(ping_response.get_response_status_code(), 200)
