# -*- coding: utf-8 -*-


class ApiError(Exception):
    def __init__(self, message, requestId=None):
        self.requestId = requestId
        self.message = message
        super().__init__(message)

    def __str__(self):
        if self.requestId is not None:
            return (
                self.__class__.__name__
                + ": "
                + self.message
                + " - RequestId: "
                + self.requestId
            )

        return self.__class__.__name__ + ": " + self.message


class BarzahlenAPIClientError(Exception):
    def __init__(self, message=""):
        self.message = message
        super().__init__(message)

    def __str__(self):
        return self.__class__.__name__ + ": " + self.message


class BarzahlenNetworkError(BarzahlenAPIClientError):
    pass


class IncorrectApiParametersError(BarzahlenAPIClientError):
    pass


class BarzahlenServerError(BarzahlenAPIClientError):
    pass


class BarzahlenClientError(BarzahlenAPIClientError):
    pass


#####################################
#  Specific errors with error_code  #
#####################################


class InvalidSignatureError(ApiError):
    """Authentication or authorization problem. Invalid signature"""

    code = "401"
    error_class = "auth"
    error_code = "invalid_signature"


class InvalidAuthError(ApiError):
    """Authentication or authorization problem. Invalid auth"""

    code = "401"
    error_class = "auth"
    error_code = "invalid_auth"


class InvalidSignatureFormatError(ApiError):
    """Authentication or authorization problem. Invalid signature format"""

    code = "401"
    error_class = "auth"
    error_code = "invalid_signature_format"


class OnlySandboxAllowedError(ApiError):
    """Authentication or authorization problem. Only Sandbox allowed"""

    code = "401"
    error_class = "auth"
    error_code = "only_sandbox_allowed"


class InvalidHostHeaderError(ApiError):
    """A general problem with the HTTP request. Invalid Host Header"""

    code = "400"
    error_class = "transport"
    error_code = "invalid_host_header"


class SlipNotFoundError(ApiError):
    """Invalid State. Slip Not Found"""

    code = "404"
    error_class = "invalid_state"
    error_code = "slip_not_found"


class SlipExpiredError(ApiError):
    """Invalid State. Slip Expired"""

    code = "400"
    error_class = "invalid_state"
    error_code = "slip_expired"


class SlipLockedError(ApiError):
    """Invalid State. Slip has been scanned in a store and not yet been paid."""

    code = "400"
    error_class = "invalid_state"
    error_code = "slip_locked"


class SlipAlreadyPaidError(ApiError):
    """Invalid State. Slip has been paid."""

    code = "400"
    error_class = "invalid_state"
    error_code = "slip_paid"


class RequestBodyTooLargeError(ApiError):
    """A general problem with the HTTP request. Request body too large"""

    code = "413"
    error_class = "transport"
    error_code = "request_body_too_large"


class InvalidQueryParamsError(ApiError):
    """The request body has an invalid format. Query params are not used at the moment."""

    code = "400"
    error_class = "invalid_format"
    error_code = "invalid_query_params"


class InvalidRequestUrlError(ApiError):
    """The request body has an invalid format. Query params are not used at the moment."""

    code = "404"
    error_class = "invalid_format"
    error_code = "invalid_request_url"


class InvalidTransactionsAmountError(ApiError):
    """Transactions amount: Does not match pattern '^-?[0-9]+\\.[0-9]+$'"""

    error_class = "invalid_parameter"
    error_code = "invalid_transactions_amount"


class InvalidSlipTypeError(ApiError):
    """Slip type not supported"""

    error_class = "invalid_parameter"
    error_code = "invalid_slip_type"


class ServerError(ApiError):
    """An internal error occurred on our systems"""

    code = "500"
    error_class = "server_error"
    error_code = "internal_server_error"


# Update slip specific errors


class RequestBodyNotValidJsonError(ApiError):
    """Invalid State. Request body is not valid"""

    code = "415"
    error_class = "invalid_state"
    error_code = "request_body_not_valid_json"


class UnknownAdditionalParameterError(ApiError):
    """Invalid State. Unknown additional parameter"""

    code = "400"
    error_class = "invalid_state"
    error_code = "unknown_additional_parameter"


class SlipInvalidatedError(ApiError):
    """Invalid State. Slip invalidated"""

    code = "400"
    error_class = "invalid_state"
    error_code = "slip_invalidated"


class CellPhoneCannotBeRemovedError(ApiError):
    """Invalid State. A previously set customer phone number
    cannot be removed, only changed."""

    code = "400"
    error_class = "invalid_state"
    error_code = "customer_cell_phone_cannot_be_removed"


class EmailCannotBeRemovedError(ApiError):
    """Invalid State. A previously set customer email
    cannot be removed, only changed."""

    code = "400"
    error_class = "invalid_state"
    error_code = "customer_email_cannot_be_removed"


class ReferenceKeyAlreadySetError(ApiError):
    """Invalid State.The slip already has a reference key set."""

    code = "400"
    error_class = "invalid_state"
    error_code = "reference_key_already_set"


class ReferenceKeyAlreadyExistsError(ApiError):
    """Invalid State.Your division is configured to not allow
    duplicate reference keys."""

    code = "400"
    error_class = "invalid_state"
    error_code = "reference_key_already_exists"


class TransactionNotFoundError(ApiError):
    """Invalid State.Transaction not found."""

    code = "400"
    error_class = "invalid_state"
    error_code = "transaction_not_found"


class RequestBodyNotAJsonObjectError(ApiError):
    """Invalid State."""

    code = "400"
    error_class = "invalid_parameter"
    error_code = "request_body_not_a_json_object"


class CellPhoneNotSetteableError(ApiError):
    """Invalid State."""

    code = "400"
    error_class = "invalid_parameter"
    error_code = "customer_cell_phone_not_settable"


class EmailNotSetteableError(ApiError):
    """Invalid State."""

    code = "400"
    error_class = "invalid_parameter"
    error_code = "customer_email_not_settable"


class ReferenceKeyNotSetteableError(ApiError):
    """Invalid State."""

    code = "400"
    error_class = "invalid_parameter"
    error_code = "reference_key_not_settable"


class TransactionsAmountNotSetteableError(ApiError):
    """Invalid State."""

    code = "400"
    error_class = "invalid_parameter"
    error_code = "transactions_amount_not_settable"


class InvalidExpiresAtError(ApiError):
    """Invalid State."""

    code = "400"
    error_class = "invalid_parameter"
    error_code = "invalid_expires_at"


class InvalidCustomerError(ApiError):
    """Invalid State."""

    code = "400"
    error_class = "invalid_parameter"
    error_code = "invalid_customer"


class InvalidCustomerEmailError(ApiError):
    """Invalid State."""

    code = "400"
    error_class = "invalid_parameter"
    error_code = "invalid_customer_email"


class InvalidReferenceKeyError(ApiError):
    """Invalid State."""

    code = "400"
    error_class = "invalid_parameter"
    error_code = "invalid_reference_key"


class InvalidCustomerCellPhoneError(ApiError):
    """Invalid State."""

    code = "400"
    error_class = "invalid_parameter"
    error_code = "invalid_customer_cell_phone"


class InvalidTransactionsError(ApiError):
    """Invalid State."""

    code = "400"
    error_class = "invalid_parameter"
    error_code = "invalid_transactions"


class InvalidTransactionIdError(ApiError):
    """Invalid State."""

    code = "400"
    error_class = "invalid_parameter"
    error_code = "invalid_transactions_id"


class TooEarlyExpiresAtError(ApiError):
    """Invalid State."""

    code = "400"
    error_class = "invalid_parameter"
    error_code = "too_early_expires_at"


class LegalAmountLimitExceededError(ApiError):
    """Not Allowed."""

    code = "403"
    error_class = "not_allowed"
    error_code = "legal_amount_limit_exceeded"


class SliptextMessageResendLimitExceededError(ApiError):
    """Not Allowed."""

    code = "403"
    error_class = "not_allowed"
    error_code = "slip_text_message_resend_limit_exceeded"


class TooLateExpiresAtError(ApiError):
    """Invalid State."""

    code = "400"
    error_class = "invalid_parameter"
    error_code = "too_late_expires_at"


class IncorrectPointParametersError(ApiError):
    """Invalid Lat/Lng parameters in V3 Stores API."""

    code = "400"
    error_class = "invalid_parameters"
    error_code = "invalid_point_parameters"


class IncorrectBoundsParametersError(ApiError):
    """Invalid Lat/Lng parameters in V3 Stores API."""

    code = "400"
    error_class = "invalid_parameters"
    error_code = "invalid_bounds_parameters"


############################################
#  More generic errors without error_code  #
############################################


class AuthenticationError(ApiError):
    """A general problem with Authorization"""

    error_class = "auth"


class TransportGenericError(ApiError):
    """A general problem with the HTTP request"""

    error_class = "transport"


class IdempotencyError(ApiError):
    """A problem concerning the idempotency mechanism"""

    code = "400"
    error_class = "idempotency"


class RateLimitError(ApiError):
    """Too many requests were sent per duration, retry the same request later"""

    code = "429"
    error_class = "rate_limit"


class InvalidStateError(ApiError):
    """The slip does not have the state necessary for the requested action"""

    error_class = "invalid_state"


class InvalidParameterError(ApiError):
    """One of the given parameters does not have the necessary format or is invalid for some other reason"""

    error_class = "invalid_parameter"


class NotAllowedError(ApiError):
    """Organizational or legal reasons prevent the requested action"""

    error_class = "not_allowed"


class UnknownError(ApiError):
    """Unknown error not covered by documented error types"""


def error_mapper(error_class, error_code):
    specific_errors_map = {
        ("auth", "invalid_signature"): InvalidSignatureError,
        ("auth", "invalid_signature_format"): InvalidSignatureFormatError,
        ("auth", "only_sandbox_allowed"): OnlySandboxAllowedError,
        ("auth", "invalid_auth"): InvalidAuthError,
        ("transport", "invalid_host_header"): InvalidHostHeaderError,
        ("transport", "request_body_too_large"): RequestBodyTooLargeError,
        ("invalid_format", "invalid_query_params"): InvalidQueryParamsError,
        ("invalid_format", "invalid_request_url"): InvalidRequestUrlError,
        (
            "invalid_parameter",
            "invalid_transactions_amount",
        ): InvalidTransactionsAmountError,
        ("invalid_parameter", "invalid_slip_type"): InvalidSlipTypeError,
        (
            "invalid_parameters",
            "invalid_point_parameters",
        ): IncorrectPointParametersError,
        (
            "invalid_parameters",
            "invalid_bounds_parameters",
        ): IncorrectBoundsParametersError,
        ("invalid_state", "slip_not_found"): SlipNotFoundError,
        ("invalid_state", "slip_expired"): SlipExpiredError,
        ("invalid_state", "slip_locked"): SlipLockedError,
        ("invalid_state", "slip_paid"): SlipAlreadyPaidError,
        ("invalid_state", "request_body_not_valid_json"): RequestBodyNotValidJsonError,
        (
            "invalid_state",
            "unknown_additional_parameter",
        ): UnknownAdditionalParameterError,
        ("invalid_state", "slip_invalidated"): SlipInvalidatedError,
        (
            "invalid_state",
            "customer_cell_phone_cannot_be_removed",
        ): CellPhoneCannotBeRemovedError,
        (
            "invalid_state",
            "customer_email_cannot_be_removed",
        ): EmailCannotBeRemovedError,
        ("invalid_state", "reference_key_already_set"): ReferenceKeyAlreadySetError,
        (
            "invalid_state",
            "reference_key_already_exists",
        ): ReferenceKeyAlreadyExistsError,
        ("invalid_state", "transaction_not_found"): TransactionNotFoundError,
        (
            "invalid_parameter",
            "request_body_not_a_json_object",
        ): RequestBodyNotAJsonObjectError,
        (
            "invalid_parameter",
            "customer_cell_phone_not_settable",
        ): CellPhoneNotSetteableError,
        ("invalid_parameter", "customer_email_not_settable"): EmailNotSetteableError,
        (
            "invalid_parameter",
            "reference_key_not_settable",
        ): ReferenceKeyNotSetteableError,
        (
            "invalid_parameter",
            "transactions_amount_not_settable",
        ): TransactionsAmountNotSetteableError,
        ("invalid_parameter", "invalid_expires_at"): InvalidExpiresAtError,
        ("invalid_parameter", "invalid_customer"): InvalidCustomerError,
        ("invalid_parameter", "invalid_customer_email"): InvalidCustomerEmailError,
        ("invalid_parameter", "invalid_reference_key"): InvalidReferenceKeyError,
        (
            "invalid_parameter",
            "invalid_customer_cell_phone",
        ): InvalidCustomerCellPhoneError,
        ("invalid_parameter", "invalid_transactions"): InvalidTransactionsError,
        ("invalid_parameter", "invalid_transactions_id"): InvalidTransactionIdError,
        ("invalid_parameter", "too_early_expires_at"): TooEarlyExpiresAtError,
        ("invalid_parameter", "too_late_expires_at"): TooLateExpiresAtError,
        ("not_allowed", "legal_amount_limit_exceeded"): LegalAmountLimitExceededError,
        (
            "not_allowed",
            "slip_text_message_resend_limit_exceeded",
        ): SliptextMessageResendLimitExceededError,
        ("server_error", "internal_server_error"): ServerError,
    }

    generic_errors_map = {
        "auth": AuthenticationError,
        "transport": TransportGenericError,
        "idempotency": IdempotencyError,
        "rate_limit": RateLimitError,
        "invalid_state": InvalidStateError,
        "invalid_parameter": InvalidParameterError,
        "not_allowed": NotAllowedError,
    }

    if (error_class, error_code) in specific_errors_map:
        return specific_errors_map[(error_class, error_code)]
    if error_class in generic_errors_map:
        return generic_errors_map[error_class]
    return UnknownError
