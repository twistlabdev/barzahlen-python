from datetime import datetime
from unittest import TestCase
import uuid

import responses

from barzahlen import error
from barzahlen.client import BarzahlenAPIClient
from tests.fixtures.barzhalen_responses import (
    create_payment_response,
    create_payout_response,
)
from tests.fixtures.barzahlen_errors import (
    invalid_slip_type_error,
    internal_server_error,
    invalid_signature_error,
    generic_transport_error,
    unknown_error,
)


class ApiBarzahlenAPICreateSlipsTestCase(TestCase):
    def test_create_payment_invalid_parameters(self):
        client = BarzahlenAPIClient("test", "test", sandbox=True)

        with self.assertRaises(error.IncorrectApiParametersError):
            client.create_payment(None, 90.50, "EUR")

    @responses.activate
    def test_create_payment_ok(self):
        response_body = create_payment_response()
        responses.add(
            responses.POST,
            "https://api-sandbox.barzahlen.de/v2/slips",
            json=response_body,
            status=201,
        )

        client = BarzahlenAPIClient("test", "test", sandbox=True)
        create_response = client.create_payment(
            "customer_key_test",
            90.50,
            "EUR",
            email="test@test.com",
            cell_phone="+41888888888",
            language="de-DE",
            expires_at=datetime.now(),
            reference_key="123456789",
            hook_url="http://localhost",
            street_and_no="Rue de Bourg 8",
            zipcode="1001",
            city="Lausanne",
            country="Switzerland",
            metadata={"internal_key": "internal_value"},
        )

        self.assertEqual(create_response.get_response_status_code(), 201)

    @responses.activate
    def test_create_payout_ok(self):
        response_body = create_payout_response()
        responses.add(
            responses.POST,
            "https://api-sandbox.barzahlen.de/v2/slips",
            json=response_body,
            status=201,
        )

        client = BarzahlenAPIClient("test", "test", sandbox=True)
        create_response = client.create_payout(
            "customer_key_test",
            -50.0,
            "EUR",
            email="test@test.com",
            cell_phone="+41888888888",
            language="de-DE",
            expires_at=datetime.now(),
            reference_key="123456789",
            hook_url="http://localhost",
            street_and_no="Rue de Bourg 8",
            zipcode="1001",
            city="Lausanne",
            country="Switzerland",
            metadata={"internal_key": "internal_value"},
        )

        self.assertEqual(create_response.get_response_status_code(), 201)

    @responses.activate
    def test_payout_nok_with_positive_amount(self):
        response_body = {}
        responses.add(
            responses.POST,
            "https://api-sandbox.barzahlen.de/v2/slips",
            json=response_body,
            status=400,
        )

        client = BarzahlenAPIClient("test", "test", sandbox=True)

        with self.assertRaises(error.IncorrectApiParametersError):
            client.create_payout(
                "customer_key_test",
                50.0,
                "EUR",
                email="test@test.com",
                cell_phone="+41888888888",
                language="de-DE",
                expires_at=datetime.now(),
                reference_key="123456789",
                hook_url="http://localhost",
                street_and_no="Rue de Bourg 8",
                zipcode="1001",
                city="Lausanne",
                country="Switzerland",
                metadata={"internal_key": "internal_value"},
            )

    @responses.activate
    def test_payment_nok_with_negative_amount(self):
        response_body = {}
        responses.add(
            responses.POST,
            "https://api-sandbox.barzahlen.de/v2/slips",
            json=response_body,
            status=400,
        )

        client = BarzahlenAPIClient("test", "test", sandbox=True)

        with self.assertRaises(error.IncorrectApiParametersError):
            client.create_payment(
                "customer_key_test",
                -50.0,
                "EUR",
                email="test@test.com",
                cell_phone="+41888888888",
                language="de-DE",
                expires_at=datetime.now(),
                reference_key="123456789",
                hook_url="http://localhost",
                street_and_no="Rue de Bourg 8",
                zipcode="1001",
                city="Lausanne",
                country="Switzerland",
                metadata={"internal_key": "internal_value"},
            )

    @responses.activate
    def test_create_payment_ok_custom_idempotency_key(self):
        response_body = create_payment_response()
        responses.add(
            responses.POST,
            "https://api-sandbox.barzahlen.de/v2/slips",
            json=response_body,
            status=201,
        )

        client = BarzahlenAPIClient("test", "test", sandbox=True)
        create_response = client.create_payment(
            "customer_key_test",
            90.50,
            "EUR",
            idempotency_key=str(uuid.uuid4()),
            email="test@test.com",
            cell_phone="+41888888888",
            language="de-DE",
            expires_at=datetime.now(),
            reference_key="123456789",
            hook_url="http://localhost",
            street_and_no="Rue de Bourg 8",
            zipcode="1001",
            city="Lausanne",
            country="Switzerland",
            metadata={"internal_key": "internal_value"},
        )

        self.assertEqual(create_response.get_response_status_code(), 201)

    @responses.activate
    def test_no_api_client_error(self):
        response_body = {}
        responses.add(
            responses.POST,
            "https://api-sandbox.barzahlen.de/v2/slips",
            json=response_body,
            status=400,
        )

        client = BarzahlenAPIClient("test", "test", sandbox=True)

        with self.assertRaises(error.BarzahlenClientError):
            client.create_payment(
                "customer_key_test", 90.50, "EUR", expires_at=datetime.now().isoformat()
            )

    @responses.activate
    def test_no_api_server_error(self):
        response_body = {}
        responses.add(
            responses.POST,
            "https://api-sandbox.barzahlen.de/v2/slips",
            json=response_body,
            status=500,
        )

        client = BarzahlenAPIClient("test", "test", sandbox=True)

        with self.assertRaises(error.BarzahlenServerError):
            client.create_payment("customer_key_test", 90.50, "EUR")

    @responses.activate
    def test_invalid_slip_type_error(self):
        response_body = invalid_slip_type_error()
        responses.add(
            responses.POST,
            "https://api-sandbox.barzahlen.de/v2/slips",
            json=response_body,
            status=400,
        )

        client = BarzahlenAPIClient("test", "test", sandbox=True)

        with self.assertRaises(error.InvalidSlipTypeError):
            client.create_payment("customer_key_test", 90.50, "EUR")

    @responses.activate
    def test_invalid_signature_error(self):
        response_body = invalid_signature_error()
        responses.add(
            responses.POST,
            "https://api-sandbox.barzahlen.de/v2/slips",
            json=response_body,
            status=401,
        )

        client = BarzahlenAPIClient("test", "test", sandbox=True)

        with self.assertRaises(error.InvalidSignatureError):
            client.create_payment("customer_key_test", 90.50, "EUR")

    @responses.activate
    def test_generic_transport_error(self):
        response_body = generic_transport_error()
        responses.add(
            responses.POST,
            "https://api-sandbox.barzahlen.de/v2/slips",
            json=response_body,
            status=400,
        )

        client = BarzahlenAPIClient("test", "test", sandbox=True)

        with self.assertRaises(error.TransportGenericError):
            client.create_payment("customer_key_test", 90.50, "EUR")

    @responses.activate
    def test_internal_server_error(self):
        response_body = internal_server_error()
        responses.add(
            responses.POST,
            "https://api-sandbox.barzahlen.de/v2/slips",
            json=response_body,
            status=500,
        )

        client = BarzahlenAPIClient("test", "test", sandbox=True)

        with self.assertRaises(error.ServerError):
            client.create_payment("customer_key_test", 90.50, "EUR")

    @responses.activate
    def test_unknown_error(self):
        response_body = unknown_error()
        responses.add(
            responses.POST,
            "https://api-sandbox.barzahlen.de/v2/slips",
            json=response_body,
            status=500,
        )

        client = BarzahlenAPIClient("test", "test", sandbox=True)

        with self.assertRaises(error.UnknownError):
            client.create_payment("customer_key_test", 90.50, "EUR")

    def test_payment_invalid_transactions(self):

        client = BarzahlenAPIClient("test", "test", sandbox=True)

        with self.assertRaisesRegex(
            error.IncorrectApiParametersError, "Amount value cannot be null"
        ):
            client.create_payment("customer_key_test", None, None)

    def test_payout_invalid_transactions(self):

        client = BarzahlenAPIClient("test", "test", sandbox=True)

        with self.assertRaisesRegex(
            error.IncorrectApiParametersError, "Amount value cannot be null"
        ):
            client.create_payout("customer_key_test", None, None)

    def test_payment_invalid_amount(self):

        client = BarzahlenAPIClient("test", "test", sandbox=True)

        with self.assertRaisesRegex(
            error.IncorrectApiParametersError, "Amount value must be a float number"
        ):
            client.create_payment("customer_key_test", 10, None)

    def test_payout_invalid_amount(self):

        client = BarzahlenAPIClient("test", "test", sandbox=True)

        with self.assertRaisesRegex(
            error.IncorrectApiParametersError, "Amount value must be a float number"
        ):
            client.create_payout("customer_key_test", 10, None)
