# -*- coding: utf-8 -*-
from .error import error_mapper
from .error import BarzahlenServerError, BarzahlenClientError


class BarzahlenResponseValidator:
    def __init__(self, response):
        self._response_data = response.json()
        self._response = response

    def validate(self):
        if "error_class" not in self._response_data:
            if 400 <= self._response.status_code < 500:
                msg = (
                    f"{self._response.status_code}"
                    " Client Error: {self._response.reason} for url: {self._response.url}"
                )
                raise BarzahlenClientError(msg)
            if 500 <= self._response.status_code < 600:
                msg = (
                    f"{self._response.status_code}"
                    " Server Error: {self._response.reason} for url: {self._response.url}"
                )
                raise BarzahlenServerError(msg)
            return

        error_class = self._response_data["error_class"]
        error_code = (
            self._response_data["error_code"]
            if "error_code" in self._response_data
            else ""
        )

        raise error_mapper(error_class, error_code)(
            self._response_data["message"],
            self._response_data["request_id"]
            if "request_id" in self._response_data
            else None,
        )
