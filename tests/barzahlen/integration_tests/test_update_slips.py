from datetime import datetime
from unittest import TestCase

import responses

from barzahlen import error
from barzahlen.client import BarzahlenAPIClient
from tests.fixtures.barzhalen_responses import (
    updated_slip_response,
    retrieved_slip_during_update_response,
)


class ApiBarzahlenAPIUpdateSlipsTestCase(TestCase):
    def test_update_slip_invalid_parameters(self):
        client = BarzahlenAPIClient("test", "test", sandbox=True)
        with self.assertRaises(error.IncorrectApiParametersError):
            client.update_slip(None)

    @responses.activate
    def test_update_slip_ok(self):
        slip_id = "slp-d8d2b41e-fd55-48fc-a1e3-b6bbc0c246e2"
        update_response_body = updated_slip_response()
        responses.add(
            responses.PATCH,
            f"https://api-sandbox.barzahlen.de/v2/slips/{slip_id}",
            json=update_response_body,
            status=200,
        )

        retrieve_response_body = retrieved_slip_during_update_response()
        responses.add(
            responses.GET,
            f"https://api-sandbox.barzahlen.de/v2/slips/{slip_id}",
            json=retrieve_response_body,
            status=200,
        )

        client = BarzahlenAPIClient("test", "test", sandbox=True)
        update_response = client.update_slip(
            slip_id,
            amount=50.0,
            reference_key="key",
            customer_email="email@email.com",
            customer_cell_phone="623423422",
            expires_at=datetime.now(),
        )

        self.assertEqual(update_response.get_response_status_code(), 200)

    @responses.activate
    def test_update_slip_no_fields(self):
        slip_id = "slp-d8d2b41e-fd55-48fc-a1e3-b6bbc0c246e2"
        update_response_body = updated_slip_response()
        responses.add(
            responses.PATCH,
            f"https://api-sandbox.barzahlen.de/v2/slips/{slip_id}",
            json=update_response_body,
            status=200,
        )

        retrieve_response_body = retrieved_slip_during_update_response()
        responses.add(
            responses.GET,
            f"https://api-sandbox.barzahlen.de/v2/slips/{slip_id}",
            json=retrieve_response_body,
            status=200,
        )

        client = BarzahlenAPIClient("test", "test", sandbox=True)

        with self.assertRaises(error.IncorrectApiParametersError):
            client.update_slip(slip_id)

    @responses.activate
    def test_update_slip_amount_not_float(self):
        slip_id = "slp-d8d2b41e-fd55-48fc-a1e3-b6bbc0c246e2"
        update_response_body = updated_slip_response()
        responses.add(
            responses.PATCH,
            f"https://api-sandbox.barzahlen.de/v2/slips/{slip_id}",
            json=update_response_body,
            status=200,
        )

        retrieve_response_body = retrieved_slip_during_update_response()
        responses.add(
            responses.GET,
            f"https://api-sandbox.barzahlen.de/v2/slips/{slip_id}",
            json=retrieve_response_body,
            status=200,
        )

        client = BarzahlenAPIClient("test", "test", sandbox=True)

        with self.assertRaises(error.IncorrectApiParametersError):
            client.update_slip(slip_id, amount="50")

    @responses.activate
    def test_update_slip_reference_key_ok(self):
        slip_id = "slp-d8d2b41e-fd55-48fc-a1e3-b6bbc0c246e2"
        update_response_body = updated_slip_response()
        responses.add(
            responses.PATCH,
            f"https://api-sandbox.barzahlen.de/v2/slips/{slip_id}",
            json=update_response_body,
            status=200,
        )

        retrieve_response_body = retrieved_slip_during_update_response()
        responses.add(
            responses.GET,
            f"https://api-sandbox.barzahlen.de/v2/slips/{slip_id}",
            json=retrieve_response_body,
            status=200,
        )

        client = BarzahlenAPIClient("test", "test", sandbox=True)

        update_response = client.update_slip(slip_id, reference_key="key")
        self.assertEqual(update_response.get_response_status_code(), 200)
