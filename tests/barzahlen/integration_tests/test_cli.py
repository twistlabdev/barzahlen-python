from unittest import TestCase

from click.testing import CliRunner
import responses

from barzahlen.cli import (
    ping,
    create_slip,
    invalidate_slip,
    retrieve_slip,
    update_slip,
    stores_nearby,
    stores_within_bounds,
)
from tests.fixtures.barzhalen_responses import (
    create_payment_response,
    invalid_transactions_amount_error,
    invalidated_slip_response,
    retrieve_invalidated_slip_response,
    updated_slip_response,
    retrieved_slip_during_update_response,
    create_payout_response,
    nearest_stores_by_gps_location,
)
from tests.fixtures.barzahlen_errors import (
    slip_already_paid_error,
    slip_not_found_error,
    invalid_point_parameters_error,
    invalid_bounds_parameters_error,
)


class CliTestCase(TestCase):
    @responses.activate
    def test_cli_ping(self):
        """Test the ping operation."""
        responses.add(
            responses.GET,
            "https://api-sandbox.barzahlen.de/v2/ping",
            json={},
            status=200,
        )
        runner = CliRunner()
        result = runner.invoke(ping, ["--api-key", "test", "--division-id", "123456"])
        self.assertEqual(result.exit_code, 0)
        self.assertTrue("Service UP. Authentication successful" in result.output)

    @responses.activate
    def test_cli_create_payment(self):
        """Test the create payment operation"""
        response_body = create_payment_response()
        responses.add(
            responses.POST,
            "https://api-sandbox.barzahlen.de/v2/slips",
            json=response_body,
            status=201,
        )
        runner = CliRunner()
        result = runner.invoke(
            create_slip,
            [
                "--api-key",
                "test",
                "--division-id",
                "123456",
                "--slip-type",
                "payment",
                "--customer-key",
                "customer@example.com",
                "--amount",
                "99.50",
                "--currency",
                "EUR",
            ],
        )
        self.assertTrue("Payment transaction succesfully created" in result.output)
        self.assertTrue("Slip number" in result.output)

    @responses.activate
    def test_cli_create_payout(self):
        """Test the create payment operation"""
        response_body = create_payout_response()
        responses.add(
            responses.POST,
            "https://api-sandbox.barzahlen.de/v2/slips",
            json=response_body,
            status=201,
        )
        runner = CliRunner()
        result = runner.invoke(
            create_slip,
            [
                "--api-key",
                "test",
                "--division-id",
                "123456",
                "--slip-type",
                "payout",
                "--customer-key",
                "customer@example.com",
                "--amount",
                "-99.50",
                "--currency",
                "EUR",
            ],
        )
        self.assertTrue("Payment transaction succesfully created" in result.output)
        self.assertTrue("Slip number" in result.output)

    @responses.activate
    def test_cli_create_payment_with_hook_url(self):
        """Test the create payment operation"""
        response_body = create_payment_response()
        responses.add(
            responses.POST,
            "https://api-sandbox.barzahlen.de/v2/slips",
            json=response_body,
            status=201,
        )
        runner = CliRunner()
        result = runner.invoke(
            create_slip,
            [
                "--api-key",
                "test",
                "--division-id",
                "123456",
                "--slip-type",
                "payment",
                "--customer-key",
                "customer@example.com",
                "--amount",
                "99.50",
                "--currency",
                "EUR",
                "--hook-url",
                "https:localhost",
            ],
        )
        self.assertTrue("Payment transaction succesfully created" in result.output)
        self.assertTrue("Slip number" in result.output)

    @responses.activate
    def test_cli_create_payout_with_hook_url(self):
        """Test the create payout operation"""
        response_body = create_payout_response()
        responses.add(
            responses.POST,
            "https://api-sandbox.barzahlen.de/v2/slips",
            json=response_body,
            status=201,
        )
        runner = CliRunner()
        result = runner.invoke(
            create_slip,
            [
                "--api-key",
                "test",
                "--division-id",
                "123456",
                "--slip-type",
                "payout",
                "--customer-key",
                "customer@example.com",
                "--amount",
                "-99.50",
                "--currency",
                "EUR",
                "--hook-url",
                "https:localhost",
            ],
        )
        self.assertTrue("Payment transaction succesfully created" in result.output)
        self.assertTrue("Slip number" in result.output)

    @responses.activate
    def test_cli_create_payment_error_not_supported(self):
        """Test the create payment operation"""
        response_body = create_payment_response()
        responses.add(
            responses.POST,
            "https://api-sandbox.barzahlen.de/v2/slips",
            json=response_body,
            status=201,
        )
        runner = CliRunner()
        result = runner.invoke(
            create_slip,
            [
                "--api-key",
                "test",
                "--division-id",
                "123456",
                "--slip-type",
                "not_supported_type",
                "--customer-key",
                "customer@example.com",
                "--amount",
                "99.50",
                "--currency",
                "EUR",
            ],
        )
        self.assertTrue("Slip type not supported" in result.output)

    @responses.activate
    def test_cli_invalidate_slip(self):
        """Test the invalidate slip operation"""
        slip_id = "slp-738c5997-fa25-4e5c-8665-eaed0e91d45f"
        response_body = invalidated_slip_response()
        responses.add(
            responses.POST,
            f"https://api-sandbox.barzahlen.de/v2/slips/{slip_id}/invalidate",
            json=response_body,
            status=200,
        )
        runner = CliRunner()
        result = runner.invoke(
            invalidate_slip,
            ["--api-key", "test", "--division-id", "123456", "--slip-id", slip_id],
        )
        self.assertTrue("Slip invalidated" in result.output)
        self.assertTrue(f"Slip number: {slip_id}" in result.output)

    @responses.activate
    def test_cli_create_payment_error_invalid_amount(self):
        """Test the create payment operation"""
        response_body = invalid_transactions_amount_error()
        responses.add(
            responses.POST,
            "https://api-sandbox.barzahlen.de/v2/slips",
            json=response_body,
            status=400,
        )
        runner = CliRunner()
        result = runner.invoke(
            create_slip,
            [
                "--api-key",
                "test",
                "--division-id",
                "123456",
                "--slip-type",
                "payment",
                "--customer-key",
                "customer@example.com",
                "--amount",
                "99",
                "--currency",
                "EUR",
            ],
        )
        self.assertTrue("There was an error creating a slip" in result.output)
        self.assertTrue("InvalidTransactionsAmountError" in result.output)

    @responses.activate
    def test_cli_invalidate_slip_already_paid(self):
        """Try to invalidate an already paid slip"""
        slip_id = "slp-59c4d390-0fbf-4c48-a659-6a3707164872"
        response_body = slip_already_paid_error()
        responses.add(
            responses.POST,
            f"https://api-sandbox.barzahlen.de/v2/slips/{slip_id}/invalidate",
            json=response_body,
            status=400,
        )

        runner = CliRunner()
        result = runner.invoke(
            invalidate_slip,
            [
                "--api-key",
                "test",
                "--division-id",
                "123456",
                "--slip-id",
                slip_id,
            ],
        )
        self.assertTrue("There was an error invalidating a slip" in result.output)
        self.assertTrue("SlipAlreadyPaidError" in result.output)

    @responses.activate
    def test_cli_retrieve_slip(self):
        """Retrieve slip"""
        slip_id = "slp-31ab7b60-3df8-4979-8045-8b1af0bee895"
        response_body = retrieve_invalidated_slip_response()
        responses.add(
            responses.GET,
            f"https://api-sandbox.barzahlen.de/v2/slips/{slip_id}",
            json=response_body,
            status=200,
        )

        runner = CliRunner()
        result = runner.invoke(
            retrieve_slip,
            [
                "--api-key",
                "test",
                "--division-id",
                "123456",
                "--slip-id",
                slip_id,
            ],
        )
        self.assertTrue("Slip retrieved" in result.output)
        self.assertTrue(f"Slip number: {slip_id}" in result.output)

    @responses.activate
    def test_cli_retrieve_slip_not_found(self):
        """Retrieve slip"""
        slip_id = "slp-not-exists"
        response_body = slip_not_found_error()
        responses.add(
            responses.GET,
            f"https://api-sandbox.barzahlen.de/v2/slips/{slip_id}",
            json=response_body,
            status=404,
        )

        runner = CliRunner()
        result = runner.invoke(
            retrieve_slip,
            [
                "--api-key",
                "test",
                "--division-id",
                "123456",
                "--slip-id",
                slip_id,
            ],
        )
        self.assertTrue("There was an error retrieving a slip" in result.output)
        self.assertTrue("SlipNotFoundError" in result.output)

    @responses.activate
    def test_cli_update_slip(self):
        """Update slip"""
        slip_id = "slp-d8d2b41e-fd55-48fc-a1e3-b6bbc0c246e2"
        update_response_body = updated_slip_response()
        responses.add(
            responses.PATCH,
            f"https://api-sandbox.barzahlen.de/v2/slips/{slip_id}",
            json=update_response_body,
            status=200,
        )

        retrieve_response_body = retrieved_slip_during_update_response()
        responses.add(
            responses.GET,
            f"https://api-sandbox.barzahlen.de/v2/slips/{slip_id}",
            json=retrieve_response_body,
            status=200,
        )

        runner = CliRunner()
        result = runner.invoke(
            update_slip,
            [
                "--api-key",
                "test",
                "--division-id",
                "123456",
                "--slip-id",
                slip_id,
                "--amount",
                50.0,
            ],
        )
        self.assertTrue("Slip updated" in result.output)
        self.assertTrue(f"Slip number: {slip_id}" in result.output)

    @responses.activate
    def test_cli_update_slip_error(self):
        """Update slip"""
        slip_id = "slp-d8d2b41e-fd55-48fc-a1e3-b6bbc0c246e2"
        update_response_body = updated_slip_response()
        responses.add(
            responses.PATCH,
            f"https://api-sandbox.barzahlen.de/v2/slips/{slip_id}",
            json=update_response_body,
            status=200,
        )

        retrieve_response_body = retrieved_slip_during_update_response()
        responses.add(
            responses.GET,
            f"https://api-sandbox.barzahlen.de/v2/slips/{slip_id}",
            json=retrieve_response_body,
            status=200,
        )

        runner = CliRunner()
        result = runner.invoke(
            update_slip,
            [
                "--api-key",
                "test",
                "--division-id",
                "123456",
                "--slip-id",
                slip_id,
            ],
        )
        self.assertTrue("There was an error updating a slip" in result.output)

    @responses.activate
    def test_cli_stores_nearby(self):
        """Get stores nearby (Stores V3 API)"""
        latitude = "46.56210727859177"
        longitude = "6.610779506027727"
        response_body = nearest_stores_by_gps_location()
        responses.add(
            responses.GET,
            f"https://stores.viafintech.com/v3/stores/nearby?lat={latitude}&lng={longitude}",
            json=response_body,
            status=200,
        )
        runner = CliRunner()
        result = runner.invoke(
            stores_nearby,
            [
                "--api-key",
                "test",
                "--stores-api-key",
                "test",
                "--division-id",
                "123456",
                "--lat",
                latitude,
                "--lng",
                longitude,
            ],
        )
        self.assertTrue(
            "Successfully retrieved the 10 nearest stores from provided location"
            in result.output
        )

    @responses.activate
    def test_cli_stores_nearby_error(self):
        """Get stores nearby (Stores V3 API)"""
        latitude = "146.56210727859177"  # invalid latitude
        longitude = "6.610779506027727"
        response_body = invalid_point_parameters_error()
        responses.add(
            responses.GET,
            f"https://stores.viafintech.com/v3/stores/nearby?lat={latitude}&lng={longitude}",
            json=response_body,
            status=400,
        )
        runner = CliRunner()
        result = runner.invoke(
            stores_nearby,
            [
                "--api-key",
                "test",
                "--stores-api-key",
                "test",
                "--division-id",
                "123456",
                "--lat",
                latitude,
                "--lng",
                longitude,
            ],
        )
        self.assertTrue(
            "IncorrectPointParametersError: The following parameters have invalid values: lat"
            in result.output
        )

    @responses.activate
    def test_cli_stores_within_bounds(self):
        """Get stores within bounds (Stores V3 API)"""
        top_left_lat = "46.552746575072426"
        top_left_lng = "6.600405404779666"
        bottom_right_lat = "46.51637469033725"
        bottom_right_lng = "6.66409736819587"
        response_body = nearest_stores_by_gps_location()
        responses.add(
            responses.GET,
            (
                f"https://stores.viafintech.com/v3/stores/within_bounds?"
                f"top_left_lat={top_left_lat}"
                f"&top_left_lng={top_left_lng}"
                f"&bottom_right_lat={bottom_right_lat}"
                f"&bottom_right_lng={bottom_right_lng}"
            ),
            json=response_body,
            status=200,
        )
        runner = CliRunner()
        result = runner.invoke(
            stores_within_bounds,
            [
                "--api-key",
                "test",
                "--stores-api-key",
                "test",
                "--division-id",
                "123456",
                "-t",
                top_left_lat,
                "-l",
                top_left_lng,
                "-b",
                bottom_right_lat,
                "-r",
                bottom_right_lng,
            ],
        )
        self.assertTrue(
            "Successfully retrieved the stores within bounds" in result.output
        )

    @responses.activate
    def test_cli_stores_within_bounds_error(self):
        """Get stores within bounds (Stores V3 API)"""
        top_left_lat = "46.552746575072426"
        top_left_lng = "6.600405404779666"
        bottom_right_lat = "46.51637469033725"
        bottom_right_lng = "226.66409736819587"  # invalid longitude
        response_body = invalid_bounds_parameters_error()
        responses.add(
            responses.GET,
            (
                f"https://stores.viafintech.com/v3/stores/within_bounds?"
                f"top_left_lat={top_left_lat}"
                f"&top_left_lng={top_left_lng}"
                f"&bottom_right_lat={bottom_right_lat}"
                f"&bottom_right_lng={bottom_right_lng}"
            ),
            json=response_body,
            status=200,
        )
        runner = CliRunner()
        result = runner.invoke(
            stores_within_bounds,
            [
                "--api-key",
                "test",
                "--stores-api-key",
                "test",
                "--division-id",
                "123456",
                "-t",
                top_left_lat,
                "-l",
                top_left_lng,
                "-b",
                bottom_right_lat,
                "-r",
                bottom_right_lng,
            ],
        )
        self.assertTrue(
            "IncorrectBoundsParametersError: The following parameters" in result.output
        )
