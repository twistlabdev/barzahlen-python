from unittest import TestCase
from unittest.mock import patch

from requests.exceptions import Timeout, ConnectionError
import responses

from barzahlen.client import BarzahlenAPIClient
from barzahlen.error import (
    IncorrectApiParametersError,
    BarzahlenNetworkError,
    BarzahlenServerError,
)
from barzahlen.config import API_HOST_SANDBOX, STORES_API_HOST


class ApiBarzahlenAPIClientTestCase(TestCase):
    def test_init_no_division_id(self):
        with self.assertRaisesRegex(
            IncorrectApiParametersError,
            "Division Id and Payment Key are mandatory fields",
        ):
            BarzahlenAPIClient(None, "test", sandbox=True)

    def test_init_no_payment_key(self):
        with self.assertRaises(IncorrectApiParametersError):
            BarzahlenAPIClient("test", None, sandbox=True)

    def test_init_sandbox_url(self):
        client = BarzahlenAPIClient("test", "test", sandbox=True)
        self.assertEqual(client.get_api_url(), API_HOST_SANDBOX)

    def test_init_stores_api_url(self):
        client = BarzahlenAPIClient("test", "test", sandbox=True)
        self.assertEqual(client.get_stores_api_url(), STORES_API_HOST)

    def test_init_with_proxy(self):
        client = BarzahlenAPIClient(
            "test",
            "test",
            proxy_url="testproxy",
            proxy_port="testproxyport",
        )

        self.assertEqual(client.get_proxies()["http"], "http://testproxy:testproxyport")

        self.assertEqual(
            client.get_proxies()["https"], "https://testproxy:testproxyport"
        )

    @patch("barzahlen.client.requests.get", side_effect=Timeout())
    def test_get_intercept_timeout_errors(self, m_get):
        client = BarzahlenAPIClient("test", "test", sandbox=True)
        with self.assertRaises(BarzahlenNetworkError):
            client.ping()

    @patch("barzahlen.client.requests.get", side_effect=ConnectionError())
    def test_get_intercept_connection_errors(self, m_get):
        client = BarzahlenAPIClient("test", "test", sandbox=True)
        with self.assertRaises(BarzahlenNetworkError):
            client.ping()

    @patch("barzahlen.client.requests.post", side_effect=Timeout())
    def test_post_intercept_timeout_errors(self, m_post):
        client = BarzahlenAPIClient("test", "test", sandbox=True)
        with self.assertRaises(BarzahlenNetworkError):
            client.create_payment("customer_key_test", 90.50, "EUR")

    @patch("barzahlen.client.requests.post", side_effect=ConnectionError())
    def test_post_intercept_connection_errors(self, m_post):
        client = BarzahlenAPIClient("test", "test", sandbox=True)
        with self.assertRaises(BarzahlenNetworkError):
            client.create_payment("customer_key_test", 90.50, "EUR")

    @responses.activate
    def test_get_intercepts_500_errors(self):
        client = BarzahlenAPIClient("test", "test", sandbox=True)
        responses.add(
            responses.GET,
            "https://api-sandbox.barzahlen.de/v2/ping",
            json={},
            status=500,
        )
        with self.assertRaises(BarzahlenServerError):
            client.ping()

    @responses.activate
    def test_post_intercepts_500_errors(self):
        client = BarzahlenAPIClient("test", "test", sandbox=True)
        responses.add(
            responses.POST,
            "https://api-sandbox.barzahlen.de/v2/slips",
            json={},
            status=500,
        )
        with self.assertRaises(BarzahlenServerError):
            client.create_payment("customer_key_test", 90.50, "EUR")
